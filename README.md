A set of "patterns" against which Godot's `InputEvent`s can be matched and sampled.

Currently, this requires features from the `master` branch of `godot-rust`,
as well as a custom build of Godot with a few small features I plan to PR.
I will update this readme when I create those PRs and godot-rust is updated.