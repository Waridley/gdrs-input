use gdnative::{
	api::{Input as GDInput, InputEvent, Viewport},
	core_types::Vector2,
	object::SubClass,
	FromVariant, ToVariant,
};
use serde::{Deserialize, Serialize};

use crate::{
	patterns::{touch::ScreenTouchPat, w_mods::mouse::MouseMotionPat},
	sampling::{EvaluateEvent, SampleError, SampleInput, SampleResult, SampleValue},
	NotNan,
};

/// A pattern that can be sampled as a `Vector2`.
#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
#[non_exhaustive] // todo: this *may* not be necessary... most esoteric inputs would probably emulate a mouse... Maybe look into VR/XR?
pub enum PointerPat {
	Mouse(MouseMotionPat),
	Touch(ScreenTouchPat),
	#[serde(other)]
	Unknown,
}

impl PointerPat {
	pub fn expand(self) -> Self {
		use PointerPat::*;

		use crate::patterns::pointer::PointerPat::Unknown;

		match self {
			Mouse(pat) => Mouse(pat.expand()),
			Touch(pat) => Touch(pat.expand()),
			Unknown => Unknown,
		}
	}
}

impl SampleInput<bool> for PointerPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<bool> {
		match self {
			PointerPat::Mouse(pat) => Ok(pat.sample(input, vp)),
			PointerPat::Touch(pat) => Ok(pat.sample(input, vp)),
			_ => Err(SampleError::UnknownPattern),
		}
	}
}

impl SampleInput<NotNan<f64>> for PointerPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<NotNan<f64>> {
		match self {
			PointerPat::Mouse(pat) => Ok(pat.sample(input, vp)),
			PointerPat::Touch(pat) => Ok(pat.sample(input, vp)),
			_ => Err(SampleError::UnknownPattern),
		}
	}
}

impl SampleInput<Vector2> for PointerPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<Vector2> {
		match self {
			PointerPat::Mouse(pat) => Ok(pat.sample(input, vp)),
			PointerPat::Touch(pat) => Ok(pat.sample(input, vp)),
			_ => Err(SampleError::UnknownPattern),
		}
	}
}

impl EvaluateEvent<bool> for PointerPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<bool>> {
		match self {
			PointerPat::Mouse(pat) => pat.try_eval_event(event),
			PointerPat::Touch(pat) => pat.try_eval_event(event),
			PointerPat::Unknown => None,
		}
	}
}

impl EvaluateEvent<NotNan<f64>> for PointerPat {
	fn try_eval_event(
		&self,
		event: &impl SubClass<InputEvent>,
	) -> Option<SampleValue<NotNan<f64>>> {
		match self {
			PointerPat::Mouse(pat) => pat.try_eval_event(event),
			PointerPat::Touch(pat) => pat.try_eval_event(event),
			PointerPat::Unknown => None,
		}
	}
}

impl EvaluateEvent<Vector2> for PointerPat {
	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<Vector2>> {
		match self {
			PointerPat::Mouse(pat) => pat.try_eval_event(event),
			PointerPat::Touch(pat) => pat.try_eval_event(event),
			PointerPat::Unknown => None,
		}
	}
}
