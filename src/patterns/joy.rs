use std::ops::RangeBounds;

use derive_more::Deref;
use gdnative::{
	api::{Input as GDInput, Viewport},
	core_types::{FromVariant, FromVariantError, GodotString, ToVariant, Variant},
	FromVariant, ToVariant,
};
use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

use crate::{
	sampling::{EvaluateEvent, IntoSampleValue, SampleInput, SampleValue},
	utils, NotNan,
};

use super::{
	ranges::{BoundPair, Expand, NotNanRange},
	PressPat,
};

#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct JoypadButtonPat {
	pub device: DeviceIndex,
	pub button_index: i64,
	pub pressed: PressPat,
}

impl JoypadButtonPat {
	#[inline]
	pub fn all_devices(button_index: i64) -> Self {
		Self {
			device: DeviceIndex::ALL,
			button_index,
			pressed: PressPat::Down,
		}
	}
}


/// Pattern to match an axis input against.
#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct JoypadMotionPat {
	pub device: DeviceIndex,
	pub axis: i64,
	pub range: NotNanRange<f64>,
}

impl JoypadMotionPat {
	#[inline]
	pub fn all_devices(axis: i64) -> Self {
		Self {
			device: DeviceIndex::ALL,
			axis,
			range: NotNanRange::unbounded(),
		}
	}

	pub fn positive(axis: i64) -> Self {
		Self {
			range: NotNanRange::positive(),
			..Self::all_devices(axis)
		}
	}

	pub fn negative(axis: i64) -> Self {
		Self {
			range: NotNanRange::negative(),
			..Self::all_devices(axis)
		}
	}

	pub fn strictly_positive(axis: i64) -> Self {
		Self {
			range: NotNanRange::strictly_positive(),
			..Self::all_devices(axis)
		}
	}

	pub fn strictly_negative(axis: i64) -> Self {
		Self {
			range: NotNanRange::strictly_negative(),
			..Self::all_devices(axis)
		}
	}

	#[inline]
	pub fn expand(self) -> Self {
		Self {
			device: self.device,
			axis: self.axis,
			range: self.range.expand(),
		}
	}
}

impl Default for JoypadMotionPat {
	#[inline]
	fn default() -> Self {
		Self {
			device: DeviceIndex::ALL,
			axis: 0,
			range: NotNanRange::unbounded(),
		}
	}
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Deref)]
pub struct DeviceIndex(i64);

impl DeviceIndex {
	pub const ALL: Self = Self(-1);

	pub fn new(idx: i64) -> Self {
		Self(idx)
	}

	pub fn get(self) -> i64 {
		self.0
	}

	pub fn matches(self, idx: i64) -> bool {
		self == Self::ALL || self.0 == idx
	}
}

impl Default for DeviceIndex {
	fn default() -> Self {
		Self::ALL
	}
}

impl From<i64> for DeviceIndex {
	fn from(idx: i64) -> Self {
		Self(idx)
	}
}

impl Serialize for DeviceIndex {
	fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		if *self == Self::ALL {
			"ALL_DEVICES".serialize(ser)
		} else {
			let guid = GDInput::godot_singleton().get_joy_guid(self.get());
			guid.to_string().as_str().serialize(ser)
		}
	}
}

impl<'de> Deserialize<'de> for DeviceIndex {
	fn deserialize<D>(de: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		let guid = <&str>::deserialize(de)?;
		if guid == "ALL_DEVICES" {
			return Ok(Self::ALL);
		}
		let guid = GodotString::from(guid);
		let input = GDInput::godot_singleton();
		let pads = input.get_connected_joypads();
		for id in pads.iter().map(|id| id.to_i64()) {
			if input.get_joy_guid(id) == guid {
				return Ok(Self(id));
			}
		}
		Err(D::Error::custom("device not connected"))
	}
}

impl ToVariant for DeviceIndex {
	fn to_variant(&self) -> Variant {
		self.0.to_variant()
	}
}

impl FromVariant for DeviceIndex {
	fn from_variant(variant: &Variant) -> Result<Self, FromVariantError> {
		i64::from_variant(variant).map(Self)
	}
}

impl SampleInput<bool> for JoypadButtonPat {
	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<bool> {
		self.pressed
			.matches_state(input.is_joy_button_pressed(self.device.get(), self.button_index))
			.in_range()
	}
}

// impl SampleInput<bool> for JoypadMotionPat {
// 	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<bool> {
// 		let value = input.get_joy_axis(self.device.get(), self.axis);
// 		let value = sampling::not_nan(value, &format!("joy axis {:?}", self));
// 		self.range.contains(&value).in_range()
// 	}
// }

impl SampleInput<NotNan<f64>> for JoypadMotionPat {
	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<NotNan<f64>> {
		let value = input.get_joy_axis(self.device.get(), self.axis);
		let value = utils::nn(value, &format!("joy axis {:?}", self));
		value.in_range_if(self.range.contains(&value))
	}
}

impl EvaluateEvent<bool> for JoypadButtonPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		event
			.is_pressed()
			.check_in_range(|state| self.pressed.matches_state(*state))
	}
}

impl EvaluateEvent<bool> for JoypadMotionPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		// TODO: Should this always return `true`, or always return `InRange(val)`?
		<Self as EvaluateEvent<NotNan<f64>>>::eval_event(self, event).convert_with(|_| true)
	}
}

impl EvaluateEvent<NotNan<f64>> for JoypadMotionPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<NotNan<f64>> {
		utils::nn(event.axis_value(), "axis value")
			.in_range_if(self.device.matches(event.device()) && self.axis == event.axis())
			.filter(|val| self.range.contains(val))
	}
}
