use gdnative::{FromVariant, ToVariant};
use serde::{Deserialize, Serialize};

use crate::patterns::ranges::IdxRange;

#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct MidiPat {
	pub channel: i64,
	pub controller: i64,
	pub instrument_range: IdxRange,
	pub message_range: IdxRange,
	pub pitch_range: IdxRange,
	pub pressure_range: IdxRange,
	pub velocity_range: IdxRange,
}
