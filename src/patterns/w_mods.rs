pub mod gestures;
pub mod key;
pub mod mouse;

use gdnative::{FromVariant, ToVariant};
use serde::{Deserialize, Serialize};

use super::PressPat;
use crate::sampling::{EvaluateEvent, IntoSampleValue, SampleValue};

#[derive(
	Copy, Clone, Default, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct ModifierKeys {
	pub alt: PressPat,
	pub command: PressPat,
	pub control: PressPat,
	pub meta: PressPat,
	pub shift: PressPat,
}

impl ModifierKeys {
	pub const IGNORE: Self = Self {
		alt: PressPat::Either,
		command: PressPat::Either,
		control: PressPat::Either,
		meta: PressPat::Either,
		shift: PressPat::Either,
	};
}

impl EvaluateEvent<bool> for ModifierKeys {
	#[inline]
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		event.is_pressed().in_range_if(
			self.alt.matches_state(event.alt())
				&& self.command.matches_state(event.command())
				&& self.control.matches_state(event.control())
				&& self.meta.matches_state(event.metakey())
				&& self.shift.matches_state(event.shift()),
		)
	}
}
