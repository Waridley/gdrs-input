use std::ops::RangeBounds;

use gdnative::{
	api::{Input as GDInput, InputEvent, Viewport},
	core_types::GodotString,
	object::SubClass,
	FromVariant, NewRef, ToVariant,
};
use serde::{Deserialize, Serialize};

use crate::{
	patterns::ranges::NN64Range,
	sampling::{EvaluateEvent, IntoSampleValue, SampleInput, SampleValue},
	utils, NN64,
};

use super::{ranges::NotNanRange, PressPat};

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant)]
pub struct ActionPat {
	pub action: GodotString,
	pub pressed: PressPat,
	pub strength_range: NN64Range, // rarely useful since you can set deadzone in editor
	pub allow_echo: bool,
}

impl Default for ActionPat {
	fn default() -> Self {
		Self {
			action: GodotString::new(),
			pressed: PressPat::Down,
			strength_range: NotNanRange::UNBOUNDED,
			allow_echo: false,
		}
	}
}

impl ActionPat {
	pub fn new(action: impl Into<GodotString>) -> Self {
		Self {
			action: action.into(),
			..Self::default()
		}
	}
}

impl SampleInput<bool> for ActionPat {
	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<bool> {
		// todo: should this check strength_range?
		self.pressed
			.matches_state(input.is_action_pressed(self.action.new_ref()))
			.in_range()
	}
}

impl SampleInput<NN64> for ActionPat {
	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<NN64> {
		let value = utils::nn(
			input.get_action_strength(self.action.new_ref()),
			"action strength",
		);
		let in_range = self.strength_range.contains(&value);
		value.in_range_if(in_range)
	}
}

impl EvaluateEvent<bool> for ActionPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<bool>> {
		// Actions are special, we need to check if the event is the *correct* action, and this can only
		// be done on `InputEvent` itself. `InputEventAction` is never fired automatically.
		let event = event.upcast();
		(event.is_action(self.action.new_ref()) && (self.allow_echo || !event.is_echo())).then(
			|| {
				self.pressed.matches_state(event.is_pressed()).in_range_if(
					self.strength_range
						.contains(&event.get_action_strength(self.action.new_ref())),
				)
			},
		)
	}
}

impl EvaluateEvent<NN64> for ActionPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN64>> {
		// Actions are special, we need to check if the event is the *correct* action, and this can only
		// be done on `InputEvent` itself. `InputEventAction` is never fired automatically.
		let event = event.upcast();
		(event.is_action(self.action.new_ref()) && (self.allow_echo || !event.is_echo())).then(
			|| {
				utils::nn(
					event.get_action_strength(self.action.new_ref()),
					"action strength",
				)
				.in_range_if(self.pressed.matches_state(event.is_pressed()))
				.and_then(|val| val.in_range_if(self.strength_range.contains(&val)))
			},
		)
	}
}
