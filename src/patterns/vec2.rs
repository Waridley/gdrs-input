use std::ops::Bound::Unbounded;

use gdnative::{api::GlobalConstants, FromVariant, ToVariant};
use serde::{Deserialize, Serialize};

use crate::patterns::{
	joy::{DeviceIndex, JoypadMotionPat},
	pointer::PointerPat,
	ranges::NotNanRange,
	w_mods::{key::KeyPat, mouse::MouseMotionPat},
	InputPat,
};

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant)]
pub enum Vec2Pat {
	HalfAxes {
		up: Option<InputPat>,
		down: Option<InputPat>,
		left: Option<InputPat>,
		right: Option<InputPat>,
	},
	FullAxes {
		x: Option<InputPat>,
		y: Option<InputPat>,
		invert_x: bool,
		invert_y: bool,
	},
	Direct {
		pat: PointerPat,
		invert_x: bool,
		invert_y: bool,
	},
}

impl Vec2Pat {
	pub const LEFT_STICK: Self = Self::FullAxes {
		x: Some(InputPat::JoypadMotion(JoypadMotionPat {
			device: DeviceIndex::ALL,
			axis: GlobalConstants::JOY_ANALOG_LX,
			range: NotNanRange(Unbounded, Unbounded),
		})),
		y: Some(InputPat::JoypadMotion(JoypadMotionPat {
			device: DeviceIndex::ALL,
			axis: GlobalConstants::JOY_ANALOG_LY,
			range: NotNanRange(Unbounded, Unbounded),
		})),
		invert_x: false,
		invert_y: false,
	};
	pub const MOUSE: Self = Self::Direct {
		pat: PointerPat::Mouse(MouseMotionPat::UNBOUNDED),
		invert_x: false,
		invert_y: false,
	};
	pub const RIGHT_STICK: Self = Self::FullAxes {
		x: Some(InputPat::JoypadMotion(JoypadMotionPat {
			device: DeviceIndex::ALL,
			axis: GlobalConstants::JOY_ANALOG_RX,
			range: NotNanRange(Unbounded, Unbounded),
		})),
		y: Some(InputPat::JoypadMotion(JoypadMotionPat {
			device: DeviceIndex::ALL,
			axis: GlobalConstants::JOY_ANALOG_RY,
			range: NotNanRange(Unbounded, Unbounded),
		})),
		invert_x: false,
		invert_y: false,
	};
	pub const WASD: Self = Self::HalfAxes {
		up: Some(InputPat::Key(KeyPat::from_scancode(GlobalConstants::KEY_W))),
		down: Some(InputPat::Key(KeyPat::from_scancode(GlobalConstants::KEY_S))),
		left: Some(InputPat::Key(KeyPat::from_scancode(GlobalConstants::KEY_A))),
		right: Some(InputPat::Key(KeyPat::from_scancode(GlobalConstants::KEY_D))),
	};

	#[inline]
	pub fn invert_x(self) -> Self {
		use Vec2Pat::*;
		match self {
			HalfAxes {
				up,
				down,
				left,
				right,
			} => Self::HalfAxes {
				up,
				down,
				left: right,
				right: left,
			},
			FullAxes {
				x,
				y,
				invert_x: _,
				invert_y,
			} => Self::FullAxes {
				x,
				y,
				invert_x: true,
				invert_y,
			},
			Direct {
				pat,
				invert_x: _,
				invert_y,
			} => Direct {
				pat,
				invert_x: true,
				invert_y,
			},
		}
	}

	#[inline]
	pub fn invert_y(self) -> Self {
		use Vec2Pat::*;
		match self {
			HalfAxes {
				up,
				down,
				left,
				right,
			} => HalfAxes {
				up: down,
				down: up,
				left,
				right,
			},
			FullAxes {
				x,
				y,
				invert_x,
				invert_y: _,
			} => FullAxes {
				x,
				y,
				invert_x,
				invert_y: true,
			},
			Direct {
				pat,
				invert_x,
				invert_y: _,
			} => Direct {
				pat,
				invert_x,
				invert_y: true,
			},
		}
	}
}
