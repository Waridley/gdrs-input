use std::ops::{Bound::*, RangeBounds};

use derive_more::{Deref, DerefMut};
use gdnative::{
	api::{GlobalConstants, Input as GDInput, Viewport},
	core_types::Vector2,
	FromVariant, ToVariant,
};
use serde::{Deserialize, Serialize};

use crate::{
	patterns::{
		ranges::{NotNanRange, NotNanVec2Range},
		w_mods::ModifierKeys,
		PressPat,
	},
	sampling::{EvaluateEvent, IntoSampleValue, SampleInput, SampleValue},
	utils, NotNan,
};

#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct MousePat {
	#[deref]
	#[deref_mut]
	pub mod_keys: ModifierKeys,
	pub button_mask: i64,
	pub pos_range: NotNanVec2Range,
}

impl MousePat {
	pub const UNBOUNDED: Self = Self {
		mod_keys: ModifierKeys::IGNORE,
		button_mask: i64::MAX,
		pos_range: NotNanVec2Range::UNBOUNDED,
	};
}

impl Default for MousePat {
	fn default() -> Self {
		Self::UNBOUNDED
	}
}

#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct MouseButtonPat {
	#[deref]
	#[deref_mut]
	pub mouse_pat: MousePat,
	pub idx: i64,
	pub pressed: PressPat,
	pub doubleclick: PressPat,
}

impl MouseButtonPat {
	pub const LEFT: Self = Self {
		mouse_pat: MousePat::UNBOUNDED,
		idx: GlobalConstants::BUTTON_LEFT,
		pressed: PressPat::Down,
		doubleclick: PressPat::Either,
	};
	pub const RIGHT: Self = Self {
		idx: GlobalConstants::BUTTON_RIGHT,
		..Self::LEFT
	};
	pub const WHEEL_DOWN: Self = Self {
		idx: GlobalConstants::BUTTON_WHEEL_DOWN,
		..Self::LEFT
	};
	pub const WHEEL_LEFT: Self = Self {
		idx: GlobalConstants::BUTTON_WHEEL_LEFT,
		..Self::LEFT
	};
	pub const WHEEL_RIGHT: Self = Self {
		idx: GlobalConstants::BUTTON_WHEEL_RIGHT,
		..Self::LEFT
	};
	pub const WHEEL_UP: Self = Self {
		idx: GlobalConstants::BUTTON_WHEEL_UP,
		..Self::LEFT
	};
	pub const XBUTTON1: Self = Self {
		idx: GlobalConstants::BUTTON_XBUTTON1,
		..Self::LEFT
	};
	pub const XBUTTON2: Self = Self {
		idx: GlobalConstants::BUTTON_XBUTTON2,
		..Self::LEFT
	};
}

impl Default for MouseButtonPat {
	fn default() -> Self {
		Self::LEFT
	}
}

/// A pattern that matches a mouse or pen input.
///
/// Godot handles pen tablets by just adding pressure and tilt properties to `InputEventMouseMotion`.
/// For a mouse, 0.0 pressure indicates button 1 is up, while 1.0 means that it is down. Tilt
/// will always be zero for a mouse.
#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct MouseMotionPat {
	#[deref]
	#[deref_mut]
	pub mouse_pat: MousePat,
	pub pressure_range: NotNanRange<f64>,
	pub tilt_range: NotNanVec2Range,
	pub src: MouseMotionVec2Source,
}

#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub enum MouseMotionVec2Source {
	Position,
	Tilt,
	Relative, // TODO: Does relative tilt exist? How to handle sampling?
}

impl Default for MouseMotionVec2Source {
	fn default() -> Self {
		Self::Position
	}
}

impl MouseMotionPat {
	pub const UNBOUNDED: Self = Self {
		mouse_pat: MousePat::UNBOUNDED,
		pressure_range: NotNanRange(Unbounded, Unbounded),
		tilt_range: NotNanVec2Range::UNBOUNDED,
		src: MouseMotionVec2Source::Position,
	};

	#[inline]
	pub fn expand(self) -> Self {
		Self {
			mouse_pat: self.mouse_pat,
			src: self.src,
			..Self::UNBOUNDED
		}
	}

	// #[inline]
	// pub fn from_rect(rect: Rect2) -> Self {
	//   Self {
	//     pos_range: NotNanVec2Range::from_rect(rect),
	//     ..Self::unbounded()
	//   }
	// }
}

impl SampleInput<bool> for ModifierKeys {
	#[inline]
	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<bool> {
		self.check_all(input).in_range()
	}
}

// impl SampleInput<bool> for MousePat {
// 	#[inline]
// 	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<bool> {
// 		(self.mod_keys.check_all(input)
// 			// && (self.button_mask & input.get_mouse_button_mask()) //TODO: not sure how to handle button_mask yet
// 			&& self.pos_range.contains(&vp.get_mouse_position()))
// 		.in_range()
// 	}
// }

// impl EvaluateEvent<bool> for MousePat {
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
// 		self.mod_keys.eval_event(event).filter(
// 			|_| self.pos_range.contains(&event.position()), // && self.button_mask == event.button_mask() // TODO: not sure how to handle button_mask yet
// 		)
// 	}
// }

impl EvaluateEvent<Vector2> for MousePat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<Vector2> {
		let mod_keys_val: SampleValue<bool> = self.mod_keys.eval_event(event);
		event.position().check_in_range(|val| {
			self.pos_range.contains(val)
				// && self.button_mask == event.button_mask() // TODO: not sure how to handle button_mask yet
				&& mod_keys_val.is_in_range()
		})
	}
}

impl SampleInput<bool> for MouseButtonPat {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<bool> {
		// note: doesn't account for doubleclick
		(input.is_mouse_button_pressed(self.idx) && self.mod_keys.check_all(input))
			.in_range_if(self.pos_range.contains(&vp.get_mouse_position()))
	}
}

impl EvaluateEvent<bool> for MouseButtonPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		self.mod_keys.eval_event(event).filter(|val| {
			self.idx == event.button_index()
				&& self.pressed.matches_state(*val)
				&& self.doubleclick.matches_state(event.is_doubleclick())
		})
	}
}

impl SampleInput<bool> for MouseMotionPat {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<bool> {
		// TODO: PR pressure and tilt getters to Godot repo
		let press = utils::nn(input.get_mouse_pressure(), "mouse pressure");
		let pos = vp.get_mouse_position();
		let tilt = input.get_mouse_tilt();
		(self.pressure_range.contains(&press) && self.mod_keys.check_all(input))
			.in_range_if(self.pos_range.contains(&pos) && self.tilt_range.contains(&tilt))
	}
}

impl SampleInput<NotNan<f64>> for MouseMotionPat {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<NotNan<f64>> {
		// TODO: PR pressure and tilt getters to Godot repo
		let pos = vp.get_mouse_position();
		let press = utils::nn(input.get_mouse_pressure(), "mouse pressure");
		let tilt = input.get_mouse_tilt();
		press.in_range_if(
			self.mod_keys.check_all(input)
				&& self.pos_range.contains(&pos)
				&& self.pressure_range.contains(&press)
				&& self.tilt_range.contains(&tilt),
		)
	}
}

impl SampleInput<Vector2> for MouseMotionPat {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<Vector2> {
		let pos = vp.get_mouse_position();
		let press = utils::nn(input.get_mouse_pressure(), "mouse pressure");
		let tilt = input.get_mouse_tilt();
		pos.in_range_if(
			self.mod_keys.check_all(input)
				&& self.pos_range.contains(&pos)
				&& self.pressure_range.contains(&press)
				&& self.tilt_range.contains(&tilt),
		)
	}
}

impl EvaluateEvent<bool> for MouseMotionPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		self.mod_keys.eval_event(event).filter(|_val| {
			self.pressure_range.contains(&event.pressure())
				&& self.tilt_range.contains(&event.tilt())
		})
	}
}

impl EvaluateEvent<NotNan<f64>> for MouseMotionPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<NotNan<f64>> {
		let mouse_pat_val: SampleValue<Vector2> = self.mouse_pat.eval_event(event);
		utils::nn(event.pressure(), "pressure").check_in_range(|val| {
			self.pressure_range.contains(val)
				&& self.tilt_range.contains(&event.tilt())
				&& mouse_pat_val.is_in_range()
		})
	}
}

impl EvaluateEvent<Vector2> for MouseMotionPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<Vector2> {
		self.mouse_pat
			.eval_event(event)
			.convert_with(|val| match self.src {
				MouseMotionVec2Source::Position => val,
				MouseMotionVec2Source::Tilt => event.tilt(),
				MouseMotionVec2Source::Relative => event.relative(),
			})
			.filter(|_val| {
				self.pressure_range.contains(&event.pressure())
					&& self.tilt_range.contains(&event.tilt())
			})
	}
}
