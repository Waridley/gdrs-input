use derive_more::{Deref, DerefMut};
use gdnative::{FromVariant, ToVariant};
use serde::{Deserialize, Serialize};

use crate::patterns::{
	ranges::{NotNanRange, NotNanVec2Range},
	w_mods::ModifierKeys,
};

#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct GesturePat {
	#[deref]
	#[deref_mut]
	pub mod_keys: ModifierKeys,
	pub pos_range: NotNanVec2Range,
}

#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct MagnifyGesturePat {
	#[deref]
	#[deref_mut]
	pub gesture_pat: GesturePat,
	pub factor_range: NotNanRange<f64>,
}

#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct PanGesturePat {
	#[deref]
	#[deref_mut]
	pub gesture_pat: GesturePat,
	pub delta_range: NotNanVec2Range,
}
