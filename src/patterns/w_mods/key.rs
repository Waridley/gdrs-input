use derive_more::{Deref, DerefMut};
use gdnative::{
	api::{Input as GDInput, Viewport},
	FromVariant, ToVariant,
};
use serde::{Deserialize, Serialize};

use crate::{
	patterns::{w_mods::ModifierKeys, PressPat},
	sampling::{EvaluateEvent, IntoSampleValue, SampleInput, SampleValue},
};

#[derive(
	Copy,
	Clone,
	Debug,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
	Deref,
	DerefMut,
)]
pub struct KeyPat {
	#[deref]
	#[deref_mut]
	pub mod_keys: ModifierKeys,
	pub scancode: i64,
	pub pressed: PressPat,
	pub allow_echo: bool,
}

impl KeyPat {
	pub const fn from_scancode(scancode: i64) -> Self {
		Self {
			mod_keys: ModifierKeys::IGNORE,
			scancode,
			pressed: PressPat::Down,
			allow_echo: false,
		}
	}
}

impl SampleInput<bool> for KeyPat {
	#[inline]
	fn sample(&self, input: &GDInput, _vp: &Viewport) -> SampleValue<bool> {
		// note: doesn't account for allow_echo because there's no `just_pressed` for non-action events
		(self.mod_keys.check_all(input) && input.is_key_pressed(self.scancode)).in_range()
	}
}

impl EvaluateEvent<bool> for KeyPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		self.mod_keys.eval_event(event).filter(|val| {
			self.scancode == event.scancode() && self.pressed.matches_state(*val) && self.allow_echo
				|| !event.is_echo()
		})
	}
}
