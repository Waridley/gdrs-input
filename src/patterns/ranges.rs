use std::{
	hash::{Hash, Hasher},
	ops::{
		Add,
		Bound::{self, *},
		RangeBounds,
	},
};

use gdnative::{
	core_types::{Dictionary, FromVariant, FromVariantError, Rect2, ToVariant, Variant, Vector2},
	prelude::Unique,
	thread_access::ThreadAccess,
	FromVariant, ToVariant,
};
use num_traits::{zero, Float, Zero};
use ordered_float::NotNan;
use serde::{Deserialize, Serialize};

use crate::utils::nn;

pub trait BoundPair<T> {
	fn from_bounds(low: Bound<T>, high: Bound<T>) -> Self;
	fn unbounded() -> Self;
	fn single(value: T) -> Self
	where
		T: Copy;
	fn inclusive(low: T, high: T) -> Self;
	fn exclusive(low: T, high: T) -> Self;
	fn positive() -> Self
	where
		T: Zero;
	fn strictly_positive() -> Self
	where
		T: Zero;
	fn negative() -> Self
	where
		T: Zero;
	fn strictly_negative() -> Self
	where
		T: Zero;
}

impl<T, R: From<(Bound<T>, Bound<T>)>> BoundPair<T> for R {
	#[inline]
	fn from_bounds(low: Bound<T>, high: Bound<T>) -> Self {
		Self::from((low, high))
	}

	#[inline]
	fn unbounded() -> Self {
		Self::from((Bound::Unbounded, Bound::Unbounded))
	}

	#[inline]
	fn single(value: T) -> Self
	where
		T: Copy,
	{
		Self::from((Included(value), Included(value)))
	}

	fn inclusive(low: T, high: T) -> Self {
		Self::from((Included(low), Included(high)))
	}

	fn exclusive(low: T, high: T) -> Self {
		Self::from((Excluded(low), Excluded(high)))
	}

	#[inline]
	fn positive() -> Self
	where
		T: Zero,
	{
		Self::from((Included(zero()), Bound::Unbounded))
	}

	#[inline]
	fn strictly_positive() -> Self
	where
		T: Zero,
	{
		Self::from((Excluded(zero()), Bound::Unbounded))
	}

	#[inline]
	fn negative() -> Self
	where
		T: Zero,
	{
		Self::from((Bound::Unbounded, Included(zero())))
	}

	#[inline]
	fn strictly_negative() -> Self
	where
		T: Zero,
	{
		Self::from((Bound::Unbounded, Excluded(zero())))
	}
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct IdxRange(Bound<i64>, Bound<i64>);

impl ToVariant for IdxRange {
	fn to_variant(&self) -> Variant {
		let start = bound_to_dict(&self.0).into_shared();
		let end = bound_to_dict(&self.1).into_shared();
		(&[start, end][..]).to_variant()
	}
}

impl FromVariant for IdxRange {
	fn from_variant(variant: &Variant) -> Result<Self, FromVariantError> {
		use gdnative::core_types::{FromVariantError::*, VariantType};
		let arr = variant.try_to_array().map_or(
			Err(InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::VariantArray,
			}),
			Ok,
		)?;

		if arr.len() != 2 {
			return Err(InvalidLength {
				len: 0,
				expected: 2,
			});
		}

		let start = arr.get(0).try_to_dictionary().as_ref().map_or(
			Err(InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::Dictionary,
			}),
			bound_from_dict,
		)?;
		let end = arr.get(1).try_to_dictionary().as_ref().map_or(
			Err(InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::Dictionary,
			}),
			bound_from_dict,
		)?;
		Ok(Self(start, end))
	}
}

impl From<(Bound<i64>, Bound<i64>)> for IdxRange {
	#[inline]
	fn from((low, high): (Bound<i64>, Bound<i64>)) -> Self {
		Self(low, high)
	}
}

impl From<IdxRange> for (Bound<i64>, Bound<i64>) {
	#[inline]
	fn from(src: IdxRange) -> (Bound<i64>, Bound<i64>) {
		(src.0, src.1)
	}
}

impl Default for IdxRange {
	#[inline]
	fn default() -> Self {
		Self::unbounded()
	}
}

fn bound_as_ref<T>(this: &Bound<T>) -> Bound<&T> {
	use std::collections::Bound::*;
	match this {
		Included(ref x) => Included(x),
		Excluded(ref x) => Excluded(x),
		Unbounded => Unbounded,
	}
}

impl RangeBounds<i64> for IdxRange {
	fn start_bound(&self) -> Bound<&i64> {
		bound_as_ref(&self.0)
	}

	fn end_bound(&self) -> Bound<&i64> {
		bound_as_ref(&self.1)
	}
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct NotNanRange<T: Float>(pub Bound<NotNan<T>>, pub Bound<NotNan<T>>);

pub type NN32Range = NotNanRange<f32>;
pub type NN64Range = NotNanRange<f64>;

fn bound_to_dict<T: ToVariant>(bound: &Bound<T>) -> Dictionary<Unique> {
	let dict = Dictionary::new();
	match bound {
		Included(val) => dict.insert("Included", val.to_variant()),
		Excluded(val) => dict.insert("Excluded", val.to_variant()),
		Unbounded => dict.insert("Unbounded", Variant::new()),
	}
	dict
}

fn bound_from_dict<T: FromVariant, Access: ThreadAccess>(
	dict: &Dictionary<Access>,
) -> Result<Bound<T>, FromVariantError> {
	use std::ops::Bound::*;

	use gdnative::core_types::{FromVariantError::*, VariantEnumRepr::ExternallyTagged};

	dict.iter().next().map_or(
		Err(InvalidEnumRepr {
			expected: ExternallyTagged,
			error: Box::new(InvalidLength {
				len: 0,
				expected: 1,
			}),
		}),
		|(k, v)| {
			let k = String::from_variant(&k)?;
			match &*k {
				"Included" => T::from_variant(&v).map(Included),
				"Excluded" => T::from_variant(&v).map(Excluded),
				"Unbounded" => Ok(Unbounded),
				_ => Err(UnknownEnumVariant {
					variant: k,
					expected: &["Included", "Excluded", "Unbounded"],
				}),
			}
		},
	)
}

fn unwrap_not_nan_bound<T: Copy>(bound: &Bound<NotNan<T>>) -> Bound<T> {
	match bound {
		Included(nn) => Included(nn.into_inner()),
		Excluded(nn) => Excluded(nn.into_inner()),
		Unbounded => Unbounded,
	}
}

impl<T: ToVariant + Float> ToVariant for NotNanRange<T> {
	fn to_variant(&self) -> Variant {
		let start = bound_to_dict(&unwrap_not_nan_bound(&self.0)).into_shared();
		let end = bound_to_dict(&unwrap_not_nan_bound(&self.1)).into_shared();
		(&[start, end][..]).to_variant()
	}
}

impl<T: FromVariant + Float> FromVariant for NotNanRange<T> {
	fn from_variant(variant: &Variant) -> Result<Self, FromVariantError> {
		use std::collections::Bound::*;

		use gdnative::core_types::{FromVariantError::*, VariantType};

		let arr = variant.try_to_array().map_or(
			Err(InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::VariantArray,
			}),
			Ok,
		)?;

		if arr.len() != 2 {
			return Err(InvalidLength {
				len: 0,
				expected: 2,
			});
		}

		let start = arr.get(0).try_to_dictionary().as_ref().map_or(
			Err(InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::Dictionary,
			}),
			bound_from_dict::<T, _>,
		)?;

		let start = match start {
			Included(val) => Included(nn(val, "start_bound")),
			Excluded(val) => Excluded(nn(val, "start_bound")),
			Unbounded => Unbounded,
		};

		let end = arr.get(1).try_to_dictionary().as_ref().map_or(
			Err(InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::Dictionary,
			}),
			bound_from_dict::<T, _>,
		)?;

		let end = match end {
			Included(val) => Included(nn(val, "start_bound")),
			Excluded(val) => Excluded(nn(val, "start_bound")),
			Unbounded => Unbounded,
		};

		Ok(Self(start, end))
	}
}

impl<T: Float + Zero> Add<Self> for NotNanRange<T> {
	type Output = Self;

	fn add(self, rhs: Self) -> Self::Output {
		use std::collections::Bound::*;
		let low = match self.0 {
			Included(low) => match rhs.0 {
				Included(r_low) => Included(low + r_low),
				Excluded(r_low) => Excluded(low + r_low),
				Unbounded => Unbounded,
			},
			Excluded(low) => match rhs.0 {
				Included(r_low) | Excluded(r_low) => Excluded(low + r_low),
				Unbounded => Unbounded,
			},
			Unbounded => Unbounded,
		};

		let high = match self.1 {
			Included(high) => match rhs.1 {
				Included(r_high) => Included(high + r_high),
				Excluded(r_high) => Excluded(high + r_high),
				Unbounded => Unbounded,
			},
			Excluded(high) => match rhs.1 {
				Included(r_high) | Excluded(r_high) => Excluded(high + r_high),
				Unbounded => Unbounded,
			},
			Unbounded => Unbounded,
		};

		Self(low, high)
	}
}

impl<T: Float + Zero> Zero for NotNanRange<T> {
	fn zero() -> Self {
		Self::exclusive(zero(), zero())
	}

	fn is_zero(&self) -> bool {
		self == &Self::zero()
	}
}

impl<T: Float + Zero> NotNanRange<T> {
	pub const UNBOUNDED: Self = Self(Unbounded, Unbounded);

	#[inline]
	pub fn deadzone(self, dz: T) -> Self {
		use std::collections::Bound::*;
		let dz = NotNan::new(dz).expect("deadzone shouldn't be `NaN`");

		let low = match self.0 {
			Included(low) | Excluded(low) => Some(low),
			Unbounded => None,
		};

		let high = match self.1 {
			Included(high) | Excluded(high) => Some(high),
			Unbounded => None,
		};

		match (low, high) {
			// both negative
			(_, Some(high)) if high <= zero() => {
				debug_assert!(low.is_none() || matches!(low, Some(low) if low < high));
				Self(self.0, Excluded(-dz))
			}
			// both positive
			(Some(low), _) if low >= zero() => {
				debug_assert!(high.is_none() || matches!(high, Some(high) if high > low));
				Self(Excluded(dz), self.1)
			}
			_ => panic!("can't determine which sign to use for deadzone"),
		}
	}
}

pub trait Expand<T>: RangeBounds<T> {
	fn expand(self) -> Self;
}

impl<R, T: Zero + PartialOrd> Expand<T> for R
where
	R: From<(Bound<T>, Bound<T>)> + RangeBounds<T>,
{
	#[inline]
	fn expand(self) -> Self {
		use std::collections::Bound::*;
		// TODO: Maybe there should be a separate function to force a full-axis?
		// TODO: Take a deadzone parameter? Or leave that to users?
		// TODO: A way to clamp the max value, or even default to 1.0?
		let low = self.start_bound();
		let high = self.end_bound();
		let low = match low {
			Included(low) | Excluded(low) => {
				if low.lt(&zero()) {
					Unbounded
				} else {
					Excluded(zero())
				}
			}
			Unbounded => Unbounded,
		};
		let high = match high {
			Included(high) | Excluded(high) => {
				if high.gt(&zero()) {
					Unbounded
				} else {
					Excluded(zero())
				}
			}
			Unbounded => Unbounded,
		};

		Self::from((low, high))
	}
}

impl<T: Float> Eq for NotNanRange<T> {}

#[allow(clippy::derive_hash_xor_eq)]
impl<T: Float> Hash for NotNanRange<T> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		(self.0, self.1).hash(state)
	}
}

impl<T: Float> From<(Bound<NotNan<T>>, Bound<NotNan<T>>)> for NotNanRange<T> {
	fn from((low, high): (Bound<NotNan<T>>, Bound<NotNan<T>>)) -> Self {
		Self(low, high)
	}
}

impl<T: Float> RangeBounds<NotNan<T>> for NotNanRange<T> {
	fn start_bound(&self) -> Bound<&NotNan<T>> {
		bound_as_ref(&self.0)
	}

	fn end_bound(&self) -> Bound<&NotNan<T>> {
		bound_as_ref(&self.1)
	}
}

impl<T: Float> RangeBounds<T> for NotNanRange<T> {
	fn start_bound(&self) -> Bound<&T> {
		use std::collections::Bound::*;
		match <Self as RangeBounds<NotNan<T>>>::start_bound(self) {
			Included(start) => Included(&**start),
			Excluded(start) => Excluded(&**start),
			Unbounded => Unbounded,
		}
	}

	fn end_bound(&self) -> Bound<&T> {
		use std::collections::Bound::*;
		match <Self as RangeBounds<NotNan<T>>>::end_bound(self) {
			Included(end) => Included(&**end),
			Excluded(end) => Excluded(&**end),
			Unbounded => Unbounded,
		}
	}
}

impl<T: Float> Default for NotNanRange<T> {
	#[inline]
	fn default() -> Self {
		Self::unbounded()
	}
}

#[derive(
	Copy, Clone, Default, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct NotNanVec2Range {
	// NotNanRange<Vector2> would be less awkward and slightly more efficient, but then pairs of
	// edges would be forced to use the same Bound variant.
	pub x_range: NotNanRange<f32>,
	pub y_range: NotNanRange<f32>,
}

impl NotNanVec2Range {
	pub const UNBOUNDED: Self = Self {
		x_range: NotNanRange(Unbounded, Unbounded),
		y_range: NotNanRange(Unbounded, Unbounded),
	};

	#[inline]
	pub fn single(pos: Vector2, source: &str) -> Self {
		Self {
			x_range: NotNanRange(
				Included(nn(pos.x, &format!("{}.x", source))),
				Included(nn(pos.x, &format!("{}.x", source))),
			),
			y_range: NotNanRange(
				Included(nn(pos.y, &format!("{}.y", source))),
				Included(nn(pos.y, &format!("{}.y", source))),
			),
		}
	}

	#[inline]
	pub fn from_rect(rect: Rect2) -> Self {
		let Rect2 {
			position: pos,
			size,
		} = rect;
		let end = Vector2 {
			x: pos.x + size.x,
			y: pos.y + size.y,
		};
		Self {
			x_range: NotNanRange(
				Included(NotNan::new(pos.x).expect("position.x is `NaN`")),
				Included(NotNan::new(end.x).expect("end.x is `NaN`")),
			),
			y_range: NotNanRange(
				Included(NotNan::new(pos.y).expect("position.y is `NaN`")),
				Included(NotNan::new(end.y).expect("end.y is `NaN`")),
			),
		}
	}

	pub fn square_around_origin(radius: f32) -> Self {
		let r = nn(radius, "radius");
		Self {
			x_range: NotNanRange::inclusive(-r, r),
			y_range: NotNanRange::inclusive(-r, r),
		}
	}

	#[inline]
	pub fn expand(self) -> Self {
		let l = match self.x_range.0 {
			Included(l) | Excluded(l) if l < zero() => Unbounded,
			Unbounded => Unbounded,
			positive => positive,
		};

		let r = match self.x_range.1 {
			Included(r) | Excluded(r) if r > zero() => Unbounded,
			Unbounded => Unbounded,
			negative => negative,
		};

		let u = match self.x_range.1 {
			Included(u) | Excluded(u) if u > zero() => Unbounded,
			Unbounded => Unbounded,
			negative => negative,
		};

		let d = match self.x_range.1 {
			Included(d) | Excluded(d) if d > zero() => Unbounded,
			Unbounded => Unbounded,
			negative => negative,
		};

		Self {
			x_range: NotNanRange(l, r),
			y_range: NotNanRange(u, d),
		}
	}

	#[inline]
	pub fn contains(&self, pos: &Vector2) -> bool {
		self.x_range.contains(&nn(pos.x, "x value")) && self.y_range.contains(&nn(pos.y, "y value"))
	}

	#[inline]
	pub fn contains_all(&self, other: &Self) -> bool {
		use std::collections::Bound::*;
		let NotNanRange(x_low, x_high) = self.x_range;
		let NotNanRange(y_low, y_high) = self.y_range;
		let NotNanRange(ox_low, ox_high) = other.x_range;
		let NotNanRange(oy_low, oy_high) = other.y_range;

		let contains_x_start = match (x_low, ox_low) {
			(Included(val), Included(other)) | (Excluded(val), Excluded(other)) => val <= other,
			(Included(val), Excluded(other)) | (Excluded(val), Included(other)) => val < other,
			(Unbounded, _) => true,
			(_, Unbounded) => return false,
		};

		let contains_y_start = match (y_low, oy_low) {
			(Included(val), Included(other)) | (Excluded(val), Excluded(other)) => val <= other,
			(Included(val), Excluded(other)) | (Excluded(val), Included(other)) => val < other,
			(Unbounded, _) => true,
			(_, Unbounded) => return false,
		};

		let contains_x_end = match (x_high, ox_high) {
			(Included(val), Included(other)) | (Excluded(val), Excluded(other)) => other <= val,
			(Included(val), Excluded(other)) | (Excluded(val), Included(other)) => other < val,
			(Unbounded, _) => true,
			(_, Unbounded) => return false,
		};

		let contains_y_end = match (y_high, oy_high) {
			(Included(val), Included(other)) | (Excluded(val), Excluded(other)) => other <= val,
			(Included(val), Excluded(other)) | (Excluded(val), Included(other)) => other < val,
			(Unbounded, _) => true,
			(_, Unbounded) => return false,
		};

		contains_x_start && contains_x_end && contains_y_start && contains_y_end
	}
}

impl Add<Self> for NotNanVec2Range {
	type Output = Self;

	fn add(self, rhs: Self) -> Self::Output {
		Self {
			x_range: self.x_range + rhs.x_range,
			y_range: self.y_range + rhs.y_range,
		}
	}
}

impl Zero for NotNanVec2Range {
	fn zero() -> Self {
		Self {
			x_range: NotNanRange::exclusive(zero(), zero()),
			y_range: NotNanRange::exclusive(zero(), zero()),
		}
	}

	fn is_zero(&self) -> bool {
		self == &Self::zero()
	}
}
