use std::ops::RangeBounds;

use gdnative::{
	api::{Input as GDInput, Viewport},
	core_types::Vector2,
	FromVariant, ToVariant,
};
use ordered_float::NotNan;
use serde::{Deserialize, Serialize};

use crate::{
	patterns::{
		ranges::{BoundPair, IdxRange, NotNanVec2Range},
		PressPat,
	},
	sampling::{EvaluateEvent, IntoSampleValue, SampleInput, SampleValue},
};

#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct ScreenDragPat {
	pub idx_range: IdxRange, // not sure if this should be a range
	pub pos_range: NotNanVec2Range,
	pub rel_range: NotNanVec2Range,
	pub speed_range: NotNanVec2Range,
	pub src: ScreenDragVec2Source,
}

#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub enum ScreenDragVec2Source {
	Position,
	Relative,
	Speed,
}

impl Default for ScreenDragVec2Source {
	fn default() -> Self {
		Self::Position
	}
}


#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
pub struct ScreenTouchPat {
	pub idx_range: IdxRange,
	pub pos_range: NotNanVec2Range,
	pub pressed: PressPat,
}

impl ScreenTouchPat {
	#[inline]
	pub fn unbounded() -> Self {
		Self {
			idx_range: BoundPair::unbounded(),
			pos_range: NotNanVec2Range::UNBOUNDED,
			pressed: PressPat::Either,
		}
	}

	#[inline]
	pub fn expand(self) -> Self {
		Self {
			pressed: self.pressed,
			..Self::unbounded()
		}
	}
}

impl EvaluateEvent<bool> for ScreenDragPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		event.is_pressed().in_range_if(
			self.idx_range.contains(&event.index())
				&& self.pos_range.contains(&event.position())
				&& self.rel_range.contains(&event.relative()),
		)
	}
}

impl EvaluateEvent<NotNan<f64>> for ScreenDragPat {
	fn eval_event(&self, _event: &Self::Event) -> SampleValue<NotNan<f64>> {
		todo!()
	}
}

impl EvaluateEvent<Vector2> for ScreenDragPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<Vector2> {
		use ScreenDragVec2Source::*;
		match self.src {
			Position => event.position(),
			Relative => event.relative(),
			Speed => event.speed(),
		}
		.in_range_if(
			self.pos_range.contains(&event.position())
				&& self.rel_range.contains(&event.relative())
				&& self.speed_range.contains(&event.speed()),
		)
	}
}

impl SampleInput<bool> for ScreenTouchPat {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<bool> {
		let pressed = input.is_touch_pressed();
		self.pressed
			.matches_state(pressed)
			.in_range_if(self.pos_range.contains(&vp.get_mouse_position()))
	}
}

impl SampleInput<Vector2> for ScreenTouchPat {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<Vector2> {
		let pos = vp.get_mouse_position();
		pos.in_range_if(
			self.pressed.matches_state(input.is_touch_pressed()) && self.pos_range.contains(&pos),
		)
	}
}

impl EvaluateEvent<bool> for ScreenTouchPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		event.is_pressed().check_in_range(|val| {
			self.pressed.matches_state(*val)
				&& self.idx_range.contains(&event.index())
				&& self.pos_range.contains(&event.position())
		})
	}
}

impl EvaluateEvent<Vector2> for ScreenTouchPat {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<Vector2> {
		event.position().check_in_range(|val| {
			self.pressed.matches_state(event.is_pressed())
				&& self.idx_range.contains(&event.index())
				&& self.pos_range.contains(val)
		})
	}
}
