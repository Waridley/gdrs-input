use gdnative::{
	api::{Input as GDInput, InputEvent, Viewport},
	core_types::Vector2,
	object::SubClass,
	FromVariant, ToVariant,
};
use serde::{Deserialize, Serialize};

use super::{joy::JoypadMotionPat, pointer::PointerPat, InputPat};
use crate::{
	sampling::{EvaluateEvent, SampleInput, SampleResult, SampleValue},
	NotNan,
};

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant)]
pub enum CursorPat {
	/// Mouse or touch input, which can be used directly as the position of the cursor.
	Pointer(PointerPat),
	/// Joystick input which can be used either as the movement vector of the cursor, or as a direct
	/// position in the case of something like a twin-stick shooter.
	Stick(JoypadMotionPat),
	/// Button input which can really only be used to direct the movement of the cursor, or if only
	/// 2 positions are possible for this direction.
	Button(InputPat),
}

impl CursorPat {
	/// Expand all contained axis patterns. See [AxisPat::expand].
	#[inline]
	pub fn expand_axes(self) -> Self {
		use CursorPat::*;
		match self {
			Pointer(pat) => Pointer(pat.expand()),
			Stick(pat) => Stick(pat.expand()),
			Button(pat) => Button(pat.expand_axes()),
		}
	}
}

impl SampleInput<bool> for CursorPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<bool> {
		match self {
			CursorPat::Pointer(pat) => pat.try_sample(input, vp),
			CursorPat::Stick(pat) => Ok(pat.sample(input, vp)),
			CursorPat::Button(pat) => pat.try_sample(input, vp),
		}
	}
}

impl SampleInput<NotNan<f64>> for CursorPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<NotNan<f64>> {
		match self {
			CursorPat::Pointer(pat) => pat.try_sample(input, vp),
			CursorPat::Stick(pat) => Ok(pat.sample(input, vp)),
			CursorPat::Button(pat) => pat.try_sample(input, vp),
		}
	}
}

impl EvaluateEvent<bool> for CursorPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<bool>> {
		match self {
			CursorPat::Pointer(pat) => pat.try_eval_event(event),
			CursorPat::Stick(pat) => pat.try_eval_event(event),
			CursorPat::Button(pat) => pat.try_eval_event(event),
		}
	}
}

impl EvaluateEvent<NotNan<f64>> for CursorPat {
	fn try_eval_event(
		&self,
		event: &impl SubClass<InputEvent>,
	) -> Option<SampleValue<NotNan<f64>>> {
		match self {
			CursorPat::Pointer(pat) => pat.try_eval_event(event),
			CursorPat::Stick(pat) => pat.try_eval_event(event),
			CursorPat::Button(pat) => pat.try_eval_event(event),
		}
	}
}

impl EvaluateEvent<Vector2> for CursorPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<Vector2>> {
		match self {
			CursorPat::Pointer(pat) => pat.try_eval_event(event),
			CursorPat::Stick(_pat) => None,
			CursorPat::Button(_pat) => None,
		}
	}
}
