use std::ops::{Deref, DerefMut, RangeBounds};

use gdnative::{
	api::{Input as GDInput, Viewport},
	core_types::Vector2,
	FromVariant, ToVariant,
};
use num_traits::{zero, Float, Signed, Zero};
use serde::{Deserialize, Serialize};

use crate::{
	patterns::ranges::{NotNanRange, NotNanVec2Range},
	sampling::{
		EvaluateEvent, EventPattern, IntoSampleValue, SampleInput, SampleResult, SampleValue,
	},
	NotNan, NN32, NN64,
};
use gdnative::{api::InputEvent, object::SubClass};

#[derive(
	Copy,
	Clone,
	Default,
	Debug,
	PartialOrd,
	Ord,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
)]
pub struct DzPat<P, Dz> {
	pub pat: P,
	pub dz: Dz,
}

impl<P, Dz: Zero> DzPat<P, Dz> {
	#[inline]
	pub fn zero(pat: P) -> Self {
		Self { pat, dz: zero() }
	}
}

pub trait WithDeadzone {
	#[inline]
	fn with_deadzone<Dz>(self, dz: Dz) -> DzPat<Self, Dz>
	where
		Self: Sized,
	{
		DzPat { pat: self, dz }
	}

	#[inline]
	fn no_deadzone<Dz: Zero>(self) -> DzPat<Self, Dz>
	where
		Self: Sized,
	{
		DzPat::zero(self)
	}
}

impl<P> WithDeadzone for P {}

// WARNING: Implementing these risks accidentally ignoring deadzone. Probably don't do it.
//
// impl<P, Dz> Deref for DzPat<P, Dz> {
// 	type Target = P;
//
// 	fn deref(&self) -> &Self::Target {
// 		&self.pat
// 	}
// }
//
// impl<P, Dz> DerefMut for DzPat<P, Dz> {
// 	fn deref_mut(&mut self) -> &mut Self::Target {
// 		&mut self.pat
// 	}
// }

#[derive(
	Copy,
	Clone,
	Default,
	Debug,
	PartialOrd,
	Ord,
	PartialEq,
	Eq,
	Hash,
	Serialize,
	Deserialize,
	ToVariant,
	FromVariant,
)]
pub struct RadialDeadzone<T>(pub T);

impl<T> RadialDeadzone<T> {
	#[inline]
	pub fn contains<U>(&self, item: Vector2) -> bool
	where
		f32: PartialOrd<T>,
	{
		item.length() <= self.0
	}
}

impl<T> Deref for RadialDeadzone<T> {
	type Target = T;

	#[inline]
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl<T> DerefMut for RadialDeadzone<T> {
	#[inline]
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

pub trait Deadzone<T> {
	fn mask(&self, value: T) -> SampleValue<T>;
}

// impl Deadzone<NN32> for NotNanRange<f32> {
// 	fn mask(&self, value: NN32) -> SampleValue<NN32> {
// 		value.in_range_if(!self.contains(&value))
// 	}
// }

// impl Deadzone<NN64> for NotNanRange<f64> {
// 	fn mask(&self, value: NN64) -> SampleValue<NN64> {
// 		value.in_range_if(!self.contains(&value))
// 	}
// }

impl<T: Float, U> Deadzone<U> for NotNanRange<T>
where
	Self: RangeBounds<NotNan<T>>,
	NotNan<T>: PartialOrd<U>,
	U: PartialOrd<NotNan<T>>,
{
	#[inline]
	fn mask(&self, value: U) -> SampleValue<U> {
		let in_range = !self.contains(&value);
		value.in_range_if(in_range)
	}
}

// impl Deadzone<f64> for NotNanRange<f64> {
// 	fn mask(&self, value: f64) -> SampleValue<f64> {
// 		value.in_range_if(!self.contains(&value))
// 	}
// }

impl<Dz> Deadzone<f32> for Dz
where
	f32: PartialOrd<Dz>,
{
	#[inline]
	fn mask(&self, value: f32) -> SampleValue<f32> {
		value.in_range_if(&value.abs() >= self)
	}
}

impl<Dz> Deadzone<f64> for Dz
where
	f64: PartialOrd<Dz>,
{
	#[inline]
	fn mask(&self, value: f64) -> SampleValue<f64> {
		value.in_range_if(&value.abs() >= self)
	}
}

impl<Dz> Deadzone<NN32> for Dz
where
	NN32: PartialOrd<Dz>,
{
	#[inline]
	fn mask(&self, value: NN32) -> SampleValue<NN32> {
		value.in_range_if(&value.abs() >= self)
	}
}

impl<Dz> Deadzone<NN64> for Dz
where
	NN64: PartialOrd<Dz>,
{
	#[inline]
	fn mask(&self, value: NN64) -> SampleValue<NN64> {
		value.in_range_if(&value.abs() > self)
	}
}

impl Deadzone<Vector2> for NotNanVec2Range {
	#[inline]
	fn mask(&self, value: Vector2) -> SampleValue<Vector2> {
		value.in_range_if(!self.contains(&value))
	}
}

impl<Dz: Deadzone<NN64>, P: SampleInput<NN64>> SampleInput<NN64> for DzPat<P, Dz> {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<NN64> {
		self.pat.sample(input, vp).deadzone(&self.dz)
	}

	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<NN64> {
		self.pat
			.try_sample(input, vp)
			.map(|val| val.deadzone(&self.dz))
	}
}

impl<Dz: Deadzone<NN64>, P: SampleInput<NN64>> SampleInput<bool> for DzPat<P, Dz> {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<bool> {
		self.pat
			.sample(input, vp)
			.convert_with(|val| self.dz.mask(val).is_out_of_range())
	}

	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<bool> {
		self.pat
			.try_sample(input, vp)
			.map(|val| val.convert_with(|val| self.dz.mask(val).is_out_of_range()))
	}
}

impl<Dz: Deadzone<Vector2>, P: SampleInput<Vector2>> SampleInput<Vector2> for DzPat<P, Dz> {
	#[inline]
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<Vector2> {
		self.pat.sample(input, vp).deadzone(&self.dz)
	}

	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<Vector2> {
		self.pat
			.try_sample(input, vp)
			.map(|val| val.deadzone(&self.dz))
	}
}

impl<P: EventPattern, Dz> EventPattern for DzPat<P, Dz> {
	type Event = P::Event;
}

impl<P, Dz> EvaluateEvent<bool> for DzPat<P, Dz>
where
	Dz: Deadzone<NN64>,
	P: EvaluateEvent<NN64>,
{
	#[inline]
	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
		self.pat
			.eval_event(event)
			.convert_with(|val| self.dz.mask(val).is_out_of_range())
	}

	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<bool>> {
		self.pat
			.try_eval_event(event)
			.map(|val| val.convert_with(|val| self.dz.mask(val).is_out_of_range()))
	}
}

impl<P, Dz> EvaluateEvent<NN32> for DzPat<P, Dz>
where
	Dz: Deadzone<NN32>,
	P: EvaluateEvent<NN32>,
{
	#[inline]
	fn eval_event(&self, event: &Self::Event) -> SampleValue<NN32> {
		self.pat.eval_event(event).deadzone(&self.dz)
	}

	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN32>> {
		self.pat
			.try_eval_event(event)
			.map(|val| val.deadzone(&self.dz))
	}
}

impl<P, Dz> EvaluateEvent<NN64> for DzPat<P, Dz>
where
	Dz: Deadzone<NN64>,
	P: EvaluateEvent<NN64>,
{
	#[inline]
	fn eval_event(&self, event: &Self::Event) -> SampleValue<NN64> {
		self.pat.eval_event(event).deadzone(&self.dz)
	}

	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN64>> {
		self.pat
			.try_eval_event(event)
			.map(|val| val.deadzone(&self.dz))
	}
}

impl<P, Dz> EvaluateEvent<f32> for DzPat<P, Dz>
where
	Dz: Deadzone<f32>,
	P: EvaluateEvent<f32>,
{
	#[inline]
	fn eval_event(&self, event: &Self::Event) -> SampleValue<f32> {
		self.pat.eval_event(event).deadzone(&self.dz)
	}

	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<f32>> {
		self.pat
			.try_eval_event(event)
			.map(|val| val.deadzone(&self.dz))
	}
}

impl<P, Dz> EvaluateEvent<Vector2> for DzPat<P, Dz>
where
	Dz: Deadzone<Vector2>,
	P: EvaluateEvent<Vector2>,
{
	#[inline]
	fn eval_event(&self, event: &Self::Event) -> SampleValue<Vector2> {
		self.pat.eval_event(event).deadzone(&self.dz)
	}

	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<Vector2>> {
		self.pat
			.try_eval_event(event)
			.map(|val| val.deadzone(&self.dz))
	}
}

// impl<P, Dz> EvaluateEvent<NN64> for DzPat<&P, &Dz>
// where Dz: Deadzone<NN64>,
//       P: EvaluateEvent<NN64>, {
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<NN64> {
// 		self.pat.eval_event(event).deadzone(&self.dz)
// 	}
//
// 	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN64>> {
// 		self.pat.try_eval_event(event).map(|val| val.deadzone(&self.dz))
// 	}
// }
//
// impl<P, Dz> EvaluateEvent<bool> for DzPat<&P, &Dz>
// where Dz: Deadzone<NN64>,
//       P: EvaluateEvent<NN64>, {
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<bool> {
// 		self.pat.eval_event(event).convert_with(|val| self.dz.mask(val).is_out_of_range())
// 	}
//
// 	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<bool>> {
// 		self.pat.try_eval_event(event).map(|val| val.convert_with(|val| self.dz.mask(val).is_out_of_range()))
// 	}
// }
//
// impl<P, Dz> EvaluateEvent<Vector2> for DzPat<&P, &Dz>
// where Dz: Deadzone<Vector2>,
//       P: EvaluateEvent<Vector2>, {
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<Vector2> {
// 		self.pat.eval_event(event).deadzone(&self.dz)
// 	}
//
// 	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<Vector2>> {
// 		self.pat.try_eval_event(event).map(|val| val.deadzone(&self.dz))
// 	}
// }

// impl<'p, 'dz, T, P, Dz> EvaluateEvent<T> for DzPat<P, Dz>
// where DzPat<&'p P, &'dz Dz>: EvaluateEvent<T> {
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<T> {
// 		let DzPat { pat, dz, } = self;
// 		DzPat { pat, dz, }.eval_event(event)
// 	}
//
// 	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<T>> {
// 		let DzPat { pat, dz, } = self;
// 		DzPat { pat, dz, }.try_eval_event(event)
// 	}
// }
