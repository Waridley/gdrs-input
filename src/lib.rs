pub mod conversions;
pub mod patterns;
pub mod sampling;
pub mod utils;

pub use ordered_float::NotNan;

use std::{
	collections::{HashMap, HashSet},
	hash::Hash,
};

use conversions::ToPattern;
use gdnative::prelude::*;
use patterns::InputPat;
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;

pub type NN64 = NotNan<f64>;
pub type NN32 = NotNan<f32>;

pub struct NNVec2 {
	pub x: NN32,
	pub y: NN32,
}

impl TryFrom<Vector2> for NNVec2 {
	type Error = ordered_float::FloatIsNan;

	fn try_from(src: Vector2) -> Result<Self, Self::Error> {
		Ok(Self {
			x: NN32::new(src.x)?,
			y: NN32::new(src.y)?,
		})
	}
}

impl From<NNVec2> for Vector2 {
	fn from(src: NNVec2) -> Self {
		Self {
			x: *src.x,
			y: *src.y,
		}
	}
}

#[derive(Clone, Serialize, Deserialize)]
pub struct InputMap<T: Eq + Hash>(HashMap<InputPat, HashSet<T>>);

impl<T: Eq + Hash> Default for InputMap<T> {
	fn default() -> Self {
		Self(HashMap::default())
	}
}

impl<T: Eq + Hash> InputMap<T> {
	#[inline]
	pub fn insert(&mut self, pat: InputPat, value: T) -> Option<HashSet<T>> {
		let mut set = HashSet::default();
		set.insert(value);
		self.0.insert(pat, set)
	}

	#[inline]
	pub fn remove(&mut self, pattern: InputPat, value: T) -> bool {
		self.0
			.get_mut(&pattern)
			.map(|set| set.remove(&value))
			.unwrap_or(false)
	}

	#[inline]
	pub fn find(&self, event: InputEvent) -> Option<&HashSet<T>> {
		// todo: optimize
		for (key, set) in self.0.iter() {
			if event.matches_pat(key) {
				return Some(set);
			}
		}
		None
	}
}
