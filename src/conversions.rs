use std::ops::RangeBounds;

use gdnative::{
	api::{
		InputEvent, InputEventAction, InputEventGesture, InputEventJoypadButton,
		InputEventJoypadMotion, InputEventKey, InputEventMIDI, InputEventMagnifyGesture,
		InputEventMouse, InputEventMouseButton, InputEventMouseMotion, InputEventPanGesture,
		InputEventScreenDrag, InputEventScreenTouch, InputEventWithModifiers,
	},
	object::SubClass,
	GodotObject,
};

use crate::{
	patterns::{
		action::ActionPat,
		joy::{DeviceIndex, JoypadButtonPat, JoypadMotionPat},
		midi::MidiPat,
		ranges::{BoundPair, Expand, IdxRange, NotNanRange, NotNanVec2Range},
		touch::{ScreenDragPat, ScreenDragVec2Source, ScreenTouchPat},
		w_mods::{
			gestures::{GesturePat, MagnifyGesturePat, PanGesturePat},
			key::KeyPat,
			mouse::{MouseButtonPat, MouseMotionPat, MouseMotionVec2Source, MousePat},
			ModifierKeys,
		},
		InputPat, PressPat,
	},
	utils::nn,
};

pub trait FromEvent {
	type Event: SubClass<InputEvent>;
	fn from_event(event: &Self::Event) -> Self;
	fn matches_event(&self, event: &Self::Event) -> bool;
}

pub trait ToPattern {
	/// Returns the most-specific InputPat that matches this event. You likely want to call some
	/// conversion functions such as `expand_axes` or `fix_gd_input_map_events` before using it
	/// to match user input.
	///
	/// The main goal of this function is to preserve as much data about
	/// the original event as possible. Ranges will be set to `(Bound::Included(value), Bound::Included(value))`
	/// and thus only match the exact original input, but this allows for the most accurate decisions
	/// about how to transform the pattern into a more useful one.
	fn pat(&self) -> InputPat;
	fn matches_pat(&self, pat: &InputPat) -> bool;
}

impl ToPattern for InputEvent {
	#[inline]
	#[rustfmt::skip] // indents to visualize hierarchy
	fn pat(&self) -> InputPat {
		macro_rules! pat {
			($T1:ty, $($T:ty,)*) => {
				if let Some(event) = self.cast::<$T1>() {
					event.pat()
				} $(else if let Some(event) = self.cast::<$T>() {
					event.pat()
				})* else {
					InputPat::Unknown
				}
			}
		}

		pat!(
			InputEventAction,
			InputEventJoypadButton,
			InputEventJoypadMotion,
			InputEventMIDI,
			InputEventScreenDrag,
			InputEventScreenTouch,

			// Subclasses must be checked first or false positives will result
					InputEventMagnifyGesture,
					InputEventPanGesture,
				InputEventGesture,

				InputEventKey,

					InputEventMouseButton,
					InputEventMouseMotion,
				InputEventMouse,

			InputEventWithModifiers,
		)
	}

	#[inline]
	#[rustfmt::skip] // indents to visualize hierarchy
	fn matches_pat(&self, pat: &InputPat) -> bool {
		use InputPat::*;

		macro_rules! match_pat {
      ($($T:tt ($p:ident) => $E:ty,)*) => {
        match pat {
          $(
            $T($p) => self.cast::<$E>().map_or(false, |e| $p.matches_event(e)),
          )*
          _ => false
        }
      }
    }

		match_pat!(
			Action(pat) => InputEvent,
			JoypadButton(pat) => InputEventJoypadButton,
			JoypadMotion(pat) => InputEventJoypadMotion,
			Midi(pat) => InputEventMIDI,
			ScreenDrag(pat) => InputEventScreenDrag,
			ScreenTouch(pat) => InputEventScreenTouch,

			// Subclasses must be checked first or false positives will result
					MagnifyGesture(pat) => InputEventMagnifyGesture,
					PanGesture(pat) => InputEventPanGesture,
			  Gesture(pat) => InputEventGesture,

			  Key(pat) => InputEventKey,

					MouseButton(pat) => InputEventMouseButton,
					MouseMotion(pat) => InputEventMouseMotion,
			  Mouse(pat) => InputEventMouse,

			WithModifiers(pat) => InputEventWithModifiers,
		)
	}
}

macro_rules! impl_conversions {
  ($($E:ty => $P:ty => $V:tt ,)*) => {
    $(
      impl From<$P> for InputPat {
        fn from(pat: $P) -> Self {
          Self::$V(pat)
        }
      }

      impl ToPattern for $E {
        #[inline]
        fn pat(&self) -> InputPat {
          InputPat::$V(FromEvent::from_event(self))
        }

        #[inline]
        fn matches_pat(&self, pat: &InputPat) -> bool {
          matches!(pat, InputPat::$V(pat) if pat.matches_event(self))
        }
      }
    )*
  }
}

impl_conversions!(
  // InputEventAction => ActionPat => Action,
  InputEventJoypadButton => JoypadButtonPat => JoypadButton,
  InputEventJoypadMotion => JoypadMotionPat => JoypadMotion,
  InputEventMIDI => MidiPat => Midi,
  InputEventScreenDrag => ScreenDragPat => ScreenDrag,
  InputEventScreenTouch => ScreenTouchPat => ScreenTouch,
  InputEventWithModifiers => ModifierKeys => WithModifiers,
  InputEventGesture => GesturePat => Gesture,
  InputEventMagnifyGesture => MagnifyGesturePat => MagnifyGesture,
  InputEventPanGesture => PanGesturePat => PanGesture,
  InputEventKey => KeyPat => Key,
  InputEventMouse => MousePat => Mouse,
  InputEventMouseButton => MouseButtonPat => MouseButton,
  InputEventMouseMotion => MouseMotionPat => MouseMotion,
);

impl FromEvent for ActionPat {
	type Event = InputEvent;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		let map = gdnative::api::InputMap::godot_singleton();
		let action = map
			.get_actions()
			.iter()
			.map(|v| v.try_to_godot_string().unwrap())
			.find(|s| unsafe { map.action_has_event(s.clone(), event.assume_shared()) })
			.unwrap_or_default();

		let strength = event.get_action_strength(action.clone());
		Self {
			action,
			pressed: PressPat::from(event.is_pressed()),
			strength_range: NotNanRange::single(nn(strength, "action strength")),
			allow_echo: event.is_echo(),
		}
	}

	#[inline]
	fn matches_event(&self, event: &Self::Event) -> bool {
		event.is_action(self.action.clone())
			&& (self.allow_echo || !event.is_echo())
			&& self.pressed.matches_state(event.is_pressed())
			&& self.strength_range.contains(&nn(
				event.get_action_strength(self.action.clone()),
				"action strength",
			))
	}
}

impl From<ActionPat> for InputPat {
	fn from(src: ActionPat) -> Self {
		Self::Action(src)
	}
}

impl FromEvent for JoypadButtonPat {
	type Event = InputEventJoypadButton;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			device: DeviceIndex::new(event.device()),
			button_index: event.button_index(),
			pressed: PressPat::from(event.is_pressed()),
		}
	}

	#[inline]
	fn matches_event(&self, event: &Self::Event) -> bool {
		(self.device == DeviceIndex::ALL || self.device.get() == event.device())
			&& self.button_index == event.button_index()
			&& self.pressed.matches_state(event.is_pressed())
	}
}

impl FromEvent for JoypadMotionPat {
	type Event = InputEventJoypadMotion;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			device: event.device().into(),
			axis: event.axis(),
			range: NotNanRange::single(nn(event.axis_value(), &format!("joy axis {:?}", event))),
		}
	}

	#[inline]
	fn matches_event(&self, event: &Self::Event) -> bool {
		(self.device == DeviceIndex::ALL || self.device.get() == event.device())
			&& self.axis == event.axis()
			&& self
				.range
				.contains(&nn(event.axis_value(), &format!("joy axis {:?}", event)))
	}
}

impl FromEvent for MidiPat {
	type Event = InputEventMIDI;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			channel: event.channel(),
			controller: event.controller_number(),
			instrument_range: IdxRange::single(event.instrument()),
			message_range: IdxRange::single(event.message()),
			pitch_range: IdxRange::single(event.pitch()),
			pressure_range: IdxRange::single(event.pressure()),
			velocity_range: IdxRange::single(event.velocity()),
		}
	}

	#[inline]
	fn matches_event(&self, event: &Self::Event) -> bool {
		self.channel == event.channel()
			&& self.controller == event.controller_number()
			&& self.instrument_range.contains(&event.instrument())
			&& self.message_range.contains(&event.message())
			&& self.pitch_range.contains(&event.pitch())
			&& self.pressure_range.contains(&event.pressure())
			&& self.velocity_range.contains(&event.velocity())
	}
}

impl FromEvent for ScreenDragPat {
	type Event = InputEventScreenDrag;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			idx_range: BoundPair::single(event.index()),
			pos_range: NotNanVec2Range::single(event.position(), "InputEventScreenDrag::position"),
			rel_range: NotNanVec2Range::single(event.relative(), "InputEventScreenDrag::relative"),
			speed_range: NotNanVec2Range::single(event.speed(), "InputEventScreenDrag::speed"),
			src: ScreenDragVec2Source::default(),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.idx_range.contains(&event.index())
			&& self.pos_range.contains(&event.position())
			&& self.rel_range.contains(&event.relative())
			&& self.speed_range.contains(&event.speed())
	}
}

impl FromEvent for ScreenTouchPat {
	type Event = InputEventScreenTouch;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			idx_range: BoundPair::single(event.index()),
			pos_range: NotNanVec2Range::single(event.position(), "InputEventScreenTouch::position"),
			pressed: PressPat::from_state(event.is_pressed()),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.pressed.matches_state(event.is_pressed())
			&& self.idx_range.contains(&event.index())
			&& self.pos_range.contains(&event.position())
	}
}

impl FromEvent for ModifierKeys {
	type Event = InputEventWithModifiers;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			// I *think* only the exact combination should match...
			// but it could also ignore extras (use `ignore_unless_down` instead of `from_state`)?
			alt: PressPat::from_state(event.alt()),
			command: PressPat::from_state(event.command()),
			control: PressPat::from_state(event.control()),
			meta: PressPat::from_state(event.metakey()),
			shift: PressPat::from_state(event.shift()),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.alt.matches_state(event.alt())
			&& self.command.matches_state(event.command())
			&& self.control.matches_state(event.control())
			&& self.meta.matches_state(event.metakey())
			&& self.shift.matches_state(event.shift())
	}
}

impl FromEvent for GesturePat {
	type Event = InputEventGesture;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			mod_keys: ModifierKeys::from_event(event),
			pos_range: NotNanVec2Range::single(event.position(), "InputEventGesture::position"),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.mod_keys.matches_event(event) && self.pos_range.contains(&event.position())
	}
}

impl FromEvent for MagnifyGesturePat {
	type Event = InputEventMagnifyGesture;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			gesture_pat: GesturePat::from_event(event),
			factor_range: NotNanRange::single(nn(event.factor(), "magnify factor")),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.gesture_pat.matches_event(event)
			&& self
				.factor_range
				.contains(&nn(event.factor(), "magnify factor"))
	}
}

impl FromEvent for PanGesturePat {
	type Event = InputEventPanGesture;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			gesture_pat: GesturePat::from_event(event),
			delta_range: NotNanVec2Range::single(event.delta(), "InputEventPanGesture::delta"),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.gesture_pat.matches_event(event) && self.delta_range.contains(&event.delta())
	}
}

impl FromEvent for KeyPat {
	type Event = InputEventKey;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		KeyPat {
			mod_keys: ModifierKeys::from_event(event),
			scancode: event.scancode(),
			pressed: event.is_pressed().into(),
			allow_echo: event.is_echo(), //unlikely, probably needs user-facing UI to enable
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.mod_keys.matches_event(event)
			&& self.scancode == event.scancode()
			&& (self.allow_echo || !event.is_echo())
	}
}

impl FromEvent for MousePat {
	type Event = InputEventMouse;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			mod_keys: ModifierKeys::from_event(event),
			button_mask: event.button_mask(),
			pos_range: NotNanVec2Range::single(event.position(), "InputEventMouse::position"),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.mod_keys.matches_event(event)
			// && self.button_mask & event.button_mask() // TODO: not sure how to handle button_mask yet
			&& self.pos_range.contains(&event.position())
	}
}

impl FromEvent for MouseButtonPat {
	type Event = InputEventMouseButton;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			mouse_pat: MousePat::from_event(event),
			idx: event.button_index(),
			pressed: event.is_pressed().into(),
			doubleclick: event.is_doubleclick().into(),
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.mouse_pat.matches_event(event)
			&& self.idx == event.button_index()
			&& self.doubleclick.matches_state(event.is_doubleclick())
	}
}

impl FromEvent for MouseMotionPat {
	type Event = InputEventMouseMotion;

	#[inline]
	fn from_event(event: &Self::Event) -> Self {
		Self {
			mouse_pat: MousePat::from_event(event),
			pressure_range: NotNanRange::single(nn(event.pressure(), "mouse pressure")),
			tilt_range: NotNanVec2Range::single(event.tilt(), "InputEventMouseMotion::tilt"),
			src: MouseMotionVec2Source::Position,
		}
	}

	fn matches_event(&self, event: &Self::Event) -> bool {
		self.mouse_pat.matches_event(event)
			&& self
				.pressure_range
				.contains(&nn(event.pressure(), "mouse pressure"))
			&& self.tilt_range.contains(&event.tilt())
	}
}

impl InputPat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		use InputPat::*;
		match self {
			JoypadMotion(pat) => JoypadMotion(pat.expand_axes()),
			ScreenDrag(pat) => ScreenDrag(pat.expand_axes()),
			ScreenTouch(pat) => ScreenTouch(pat.expand_axes()),
			Gesture(pat) => Gesture(pat.expand_axes()),
			MagnifyGesture(pat) => MagnifyGesture(pat.expand_axes()),
			PanGesture(pat) => PanGesture(pat.expand_axes()),
			Mouse(pat) => Mouse(pat.expand_axes()),
			MouseButton(pat) => MouseButton(pat.expand_axes()),
			MouseMotion(pat) => MouseMotion(pat.expand_axes()),
			other => other,
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		use InputPat::*;
		match self {
			Action(pat) => Action(pat.ignore_up()),
			JoypadButton(pat) => JoypadButton(pat.ignore_up()),
			Midi(pat) => Midi(pat.ignore_up()),
			ScreenTouch(pat) => ScreenTouch(pat.ignore_up()),
			WithModifiers(pat) => WithModifiers(pat.ignore_up()),
			Gesture(pat) => Gesture(pat.ignore_up()),
			MagnifyGesture(pat) => MagnifyGesture(pat.ignore_up()),
			PanGesture(pat) => PanGesture(pat.ignore_up()),
			Key(pat) => Key(pat.ignore_up()),
			Mouse(pat) => Mouse(pat.ignore_up()),
			MouseButton(pat) => MouseButton(pat.ignore_up()),
			MouseMotion(pat) => MouseMotion(pat.ignore_up()),
			other => other,
		}
	}

	#[inline]
	pub fn fix_gd_input_map_events(self) -> Self {
		use InputPat::*;
		match self.expand_axes() {
			Action(pat) => Action(pat.fix_gd_input_map_buttons()),
			JoypadButton(pat) => JoypadButton(pat.fix_gd_input_map_buttons()),
			Midi(pat) => Midi(pat.fix_gd_input_map_buttons()),
			ScreenTouch(pat) => ScreenTouch(pat.fix_gd_input_map_buttons()),
			WithModifiers(pat) => WithModifiers(pat.fix_gd_input_map_buttons()),
			Gesture(pat) => Gesture(pat.fix_gd_input_map_buttons()),
			MagnifyGesture(pat) => MagnifyGesture(pat.fix_gd_input_map_buttons()),
			PanGesture(pat) => PanGesture(pat.fix_gd_input_map_buttons()),
			Key(pat) => Key(pat.fix_gd_input_map_buttons()),
			Mouse(pat) => Mouse(pat.fix_gd_input_map_buttons()),
			MouseButton(pat) => MouseButton(pat.fix_gd_input_map_buttons()),
			MouseMotion(pat) => MouseMotion(pat.fix_gd_input_map_buttons()),
			other => other,
		}
	}
}

impl ActionPat {
	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			pressed: self.pressed.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			pressed: PressPat::Down,
			..self
		}
	}
}

impl JoypadButtonPat {
	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			pressed: self.pressed.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			pressed: PressPat::Down,
			..self
		}
	}
}

impl JoypadMotionPat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			range: self.range.expand(),
			..self
		}
	}
}

impl MidiPat {
	#[inline]
	pub fn ignore_up(self) -> Self {
		todo!("should MidiPat have a `pressed` field?")
	}

	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			instrument_range: self.instrument_range.expand(),
			message_range: self.message_range.expand(),
			pitch_range: self.pitch_range.expand(),
			pressure_range: self.pressure_range.expand(),
			velocity_range: self.velocity_range.expand(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		todo!("should MidiPat have a `pressed` field?")
	}
}

impl ScreenDragPat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			idx_range: BoundPair::unbounded(),
			pos_range: NotNanVec2Range::UNBOUNDED,
			rel_range: NotNanVec2Range::UNBOUNDED,
			speed_range: NotNanVec2Range::UNBOUNDED,
			src: ScreenDragVec2Source::Position,
		}
	}
}

impl ScreenTouchPat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			idx_range: BoundPair::unbounded(),
			pos_range: NotNanVec2Range::UNBOUNDED,
			..self
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			pressed: self.pressed.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			pressed: PressPat::Down,
			..self
		}
	}
}

impl ModifierKeys {
	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			alt: self.alt.ignore_up(),
			command: self.command.ignore_up(),
			control: self.control.ignore_up(),
			meta: self.meta.ignore_up(),
			shift: self.shift.ignore_up(),
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		self.ignore_up()
	}
}

impl GesturePat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			pos_range: NotNanVec2Range::UNBOUNDED,
			..self
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			mod_keys: self.mod_keys.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			mod_keys: self.mod_keys.ignore_up(),
			..self
		}
	}
}

impl MagnifyGesturePat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			gesture_pat: self.gesture_pat.expand_axes(),
			factor_range: self.factor_range.expand(),
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			gesture_pat: self.gesture_pat.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			gesture_pat: self.gesture_pat.fix_gd_input_map_buttons(),
			..self
		}
	}
}

impl PanGesturePat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			gesture_pat: self.gesture_pat.expand_axes(),
			delta_range: NotNanVec2Range::UNBOUNDED,
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			gesture_pat: self.gesture_pat.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			gesture_pat: self.gesture_pat.fix_gd_input_map_buttons(),
			..self
		}
	}
}

impl KeyPat {
	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			mod_keys: self.mod_keys.ignore_up(),
			pressed: self.pressed.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			mod_keys: self.mod_keys.fix_gd_input_map_buttons(),
			pressed: PressPat::Down,
			..self
		}
	}
}

impl MousePat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			pos_range: NotNanVec2Range::UNBOUNDED,
			..self
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			mod_keys: self.mod_keys.ignore_up(),
			button_mask: 0, // TODO: what to do with this?
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			mod_keys: self.mod_keys.fix_gd_input_map_buttons(),
			..self
		}
	}
}

impl MouseButtonPat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			mouse_pat: self.mouse_pat.expand_axes(),
			..self
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			mouse_pat: self.mouse_pat.ignore_up(),
			doubleclick: self.doubleclick.ignore_up(), // TODO: does doing the opposite for doubleclick make sense?
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			mouse_pat: MousePat {
				mod_keys: self.mod_keys.ignore_up(),
				..self.mouse_pat
			},
			pressed: PressPat::Down,
			..self
		}
	}
}

impl MouseMotionPat {
	#[inline]
	pub fn expand_axes(self) -> Self {
		Self {
			mouse_pat: self.mouse_pat.expand_axes(),
			pressure_range: self.pressure_range.expand(),
			tilt_range: self.tilt_range.expand(),
			src: self.src,
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		Self {
			mouse_pat: self.mouse_pat.ignore_up(),
			..self
		}
	}

	#[inline]
	pub fn fix_gd_input_map_buttons(self) -> Self {
		Self {
			mouse_pat: MousePat {
				mod_keys: ModifierKeys::default(),
				..self.mouse_pat
			},
			..self
		}
	}
}
