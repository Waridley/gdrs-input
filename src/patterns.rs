pub mod action;
// pub mod cursor;
pub mod deadzone;
pub mod joy;
pub mod midi;
pub mod pointer;
pub mod ranges;
pub mod touch;
pub mod vec2;
pub mod w_mods;

use std::hash::Hash;

use action::ActionPat;
use gdnative::{
	api::{Input as GDInput, Viewport},
	prelude::*,
};
use joy::{JoypadButtonPat, JoypadMotionPat};
use midi::MidiPat;
use serde::{Deserialize, Serialize};
use touch::{ScreenDragPat, ScreenTouchPat};
use w_mods::{
	gestures::{GesturePat, MagnifyGesturePat, PanGesturePat},
	key::KeyPat,
	mouse::{MouseButtonPat, MouseMotionPat, MousePat},
	ModifierKeys,
};

use crate::{
	patterns::{
		deadzone::{Deadzone, DzPat},
		ranges::NN64Range,
	},
	sampling::{
		cant_sample, EvaluateEvent, EventPattern, SampleError, SampleInput, SampleResult,
		SampleValue,
	},
	NN32, NN64,
};

// TODO: AR/VR support? There don't seem to be any events related to VR, but there are classes that
//  can be sampled. VR controllers work with the Joypad system, so at least there should be *some*
//  support provided by this crate automatically.

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant)]
#[non_exhaustive]
pub enum InputPat {
	Action(ActionPat), // currently the only variant preventing `Copy`, but drag/gestures might in the future anyway...
	JoypadButton(JoypadButtonPat),
	JoypadMotion(JoypadMotionPat),
	Midi(MidiPat),
	ScreenDrag(ScreenDragPat),
	ScreenTouch(ScreenTouchPat),
	WithModifiers(ModifierKeys),
	Gesture(GesturePat),
	MagnifyGesture(MagnifyGesturePat),
	PanGesture(PanGesturePat),
	Key(KeyPat),
	Mouse(MousePat),
	MouseButton(MouseButtonPat),
	MouseMotion(MouseMotionPat),
	/// An InputEvent type that this crate does not recognize, either returned from an unknown Godot
	/// version, or deserialized from unrecognized data.
	///
	/// This is distinct from the `_` pattern enforced by the `non_exhaustive` attribute, which is
	/// used to enable support of other types in the future without breaking existing client versions.
	#[serde(other)]
	Unknown,
}

pub type Analog<Dz = NN64Range> = DzPat<InputPat, Dz>;

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant)]
pub enum ButtonPat<Dz = NN64Range> {
	Action(ActionPat),
	JoypadButton(JoypadButtonPat),
	ScreenTouch(ScreenTouchPat),
	ModKeys(ModifierKeys),
	Key(KeyPat),
	MouseButton(MouseButtonPat),
	Analog(Analog<Dz>),
}

impl SampleInput<f32> for InputPat {
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<f32> {
		<Self as SampleInput<f64>>::sample(self, input, vp).convert_with(|val| val as f32)
	}

	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<f32> {
		<Self as SampleInput<f64>>::try_sample(self, input, vp)
			.map(|val| val.convert_with(|val| val as f32))
	}
}

impl SampleInput<f64> for InputPat {
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<f64> {
		<Self as SampleInput<NN64>>::sample(self, input, vp).convert_with(|val| *val)
	}

	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<f64> {
		<Self as SampleInput<NN64>>::try_sample(self, input, vp)
			.map(|val| val.convert_with(|val| *val))
	}
}

impl<Dz: Deadzone<NN64>> SampleInput<bool> for ButtonPat<Dz> {
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<bool> {
		match self {
			ButtonPat::Action(pat) => pat.sample(input, vp),
			ButtonPat::JoypadButton(pat) => pat.sample(input, vp),
			ButtonPat::ScreenTouch(pat) => pat.sample(input, vp),
			ButtonPat::ModKeys(pat) => pat.sample(input, vp),
			ButtonPat::Key(pat) => pat.sample(input, vp),
			ButtonPat::MouseButton(pat) => pat.sample(input, vp),
			ButtonPat::Analog(analog) => {
				<InputPat as SampleInput<NN64>>::sample(&analog.pat, input, vp)
					.convert_with(|val| analog.dz.mask(val).is_out_of_range())
			}
		}
	}

	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<bool> {
		match self {
			ButtonPat::Action(pat) => pat.try_sample(input, vp),
			ButtonPat::JoypadButton(pat) => pat.try_sample(input, vp),
			ButtonPat::ScreenTouch(pat) => pat.try_sample(input, vp),
			ButtonPat::ModKeys(pat) => pat.try_sample(input, vp),
			ButtonPat::Key(pat) => pat.try_sample(input, vp),
			ButtonPat::MouseButton(pat) => pat.try_sample(input, vp),
			ButtonPat::Analog(analog) => {
				<InputPat as SampleInput<NN64>>::try_sample(&analog.pat, input, vp)
					.map(|val| val.convert_with(|val| analog.dz.mask(val).is_out_of_range()))
			}
		}
	}
}

impl<Dz> EventPattern for ButtonPat<Dz> {
	type Event = InputEvent;
}

impl<Dz: Deadzone<NN64>> EvaluateEvent<bool> for ButtonPat<Dz> {
	#[inline]
	fn try_eval_event(&self, event: &impl SubClass<Self::Event>) -> Option<SampleValue<bool>> {
		match self {
			ButtonPat::Action(pat) => pat.try_eval_event(event),
			ButtonPat::JoypadButton(pat) => pat.try_eval_event(event),
			ButtonPat::ScreenTouch(pat) => pat.try_eval_event(event),
			ButtonPat::ModKeys(pat) => pat.try_eval_event(event),
			ButtonPat::Key(pat) => pat.try_eval_event(event),
			ButtonPat::MouseButton(pat) => pat.try_eval_event(event),
			ButtonPat::Analog(analog) => {
				<InputPat as EvaluateEvent<NN64>>::try_eval_event(&analog.pat, event)
					.map(|val| val.convert_with(|val| analog.dz.mask(val).is_out_of_range()))
			}
		}
	}
}

#[derive(
	Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, ToVariant, FromVariant,
)]
#[repr(i8)]
pub enum PressPat {
	Either = -1, // All? Either? Any?
	Up = 0,
	Down = 1,
}

impl Default for PressPat {
	#[inline]
	fn default() -> Self {
		Self::Either
	}
}

impl From<bool> for PressPat {
	#[inline]
	fn from(pressed: bool) -> Self {
		Self::from_state(pressed)
	}
}

impl From<Option<bool>> for PressPat {
	#[inline]
	fn from(src: Option<bool>) -> Self {
		match src {
			Some(true) => Self::Down,
			Some(false) => Self::Up,
			None => Self::Either,
		}
	}
}

impl PressPat {
	#[inline]
	pub fn from_state(pressed: bool) -> Self {
		if pressed {
			Self::Down
		} else {
			Self::Up
		}
	}

	#[inline]
	pub fn ignore_up(self) -> Self {
		match self {
			Self::Up => Self::Either,
			other => other,
		}
	}

	#[inline]
	pub fn ignore_down(self) -> Self {
		match self {
			Self::Down => Self::Either,
			other => other,
		}
	}

	#[inline]
	pub fn matches_state(&self, pressed: bool) -> bool {
		match self {
			Self::Either => true,
			Self::Down => pressed,
			Self::Up => !pressed,
		}
	}
}

impl SampleInput<bool> for InputPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<bool> {
		match self {
			InputPat::Action(pat) => Ok(pat.sample(input, vp)),
			InputPat::JoypadButton(pat) => Ok(pat.sample(input, vp)),
			// InputPat::JoypadMotion(pat) => Ok(pat.sample(input, vp)), // see last arm
			InputPat::Midi(_pat) => Err(cant_sample("no MIDI functions in Input singleton")),
			InputPat::ScreenDrag(_pat) => Err(cant_sample("ScreenDrag is inherently dynamic")),
			InputPat::ScreenTouch(pat) => Ok(pat.sample(input, vp)),
			InputPat::WithModifiers(pat) => Ok(pat.sample(input, vp)),
			InputPat::Gesture(_pat) => Err(cant_sample("Gestures are inherently dynamic")),
			InputPat::MagnifyGesture(_pat) => {
				Err(cant_sample("MagnifyGesture is inherently dynamic"))
			}
			InputPat::PanGesture(_pat) => Err(cant_sample("PanGesture is inherently dynamic")),
			InputPat::Key(pat) => Ok(pat.sample(input, vp)),
			InputPat::Mouse(_pat) => {
				Err(cant_sample("not sure how to sample a bool from MousePat"))
			}
			InputPat::MouseButton(pat) => Ok(pat.sample(input, vp)),
			// InputPat::MouseMotion(pat) => Ok(pat.sample(input, vp)), // see last arm
			InputPat::Unknown => Err(SampleError::UnknownPattern),
			_ => Err(cant_sample(
				"pattern must be wrapped in a `DzPat` to sample as a bool",
			)),
		}
	}
}

impl SampleInput<NN64> for InputPat {
	#[inline]
	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<NN64> {
		match self {
			InputPat::Action(pat) => Ok(pat.sample(input, vp)),
			InputPat::JoypadButton(pat) => pat.try_sample(input, vp),
			InputPat::JoypadMotion(pat) => pat.try_sample(input, vp),
			InputPat::Midi(_pat) => Err(cant_sample("no MIDI functions in Input singleton")),
			InputPat::ScreenDrag(_pat) => Err(cant_sample("ScreenDrag is inherently dynamic")),
			InputPat::ScreenTouch(_pat) => Err(cant_sample(
				"not sure how to return single f64 from ScreenTouch",
			)),
			InputPat::WithModifiers(pat) => pat.try_sample(input, vp),
			InputPat::Gesture(_pat) => Err(cant_sample("Gestures are inherently dynamic")),
			InputPat::MagnifyGesture(_pat) => {
				Err(cant_sample("MagnifyGesture is inherently dynamic"))
			}
			InputPat::PanGesture(_pat) => Err(cant_sample("PanGesture is inherently dynamic")),
			InputPat::Key(pat) => pat.try_sample(input, vp),
			InputPat::Mouse(_pat) => Err(cant_sample(
				"not sure how to sample a Float from a MousePat",
			)),
			InputPat::MouseButton(pat) => pat.try_sample(input, vp),
			InputPat::MouseMotion(pat) => pat.try_sample(input, vp),
			InputPat::Unknown => Err(SampleError::UnknownPattern),
		}
	}
}

impl EvaluateEvent<bool> for InputPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<bool>> {
		use InputPat::*;
		match self {
			Action(pat) => pat.try_eval_event(event),
			JoypadButton(pat) => pat.try_eval_event(event),
			// JoypadMotion(pat) => pat.try_eval_event(event),
			Midi(_pat) => todo!("eval MIDI events"),
			// ScreenDrag(pat) => pat.try_eval_event(event),
			ScreenTouch(pat) => pat.try_eval_event(event),
			WithModifiers(pat) => pat.try_eval_event(event),
			// Gesture(pat) => pat.try_eval_event(event),
			// MagnifyGesture(pat) => pat.try_eval_event(event),
			// PanGesture(pat) => pat.try_eval_event(event),
			Key(pat) => pat.try_eval_event(event),
			// Mouse(pat) => pat.try_eval_event(event),
			MouseButton(pat) => pat.try_eval_event(event),
			// MouseMotion(pat) => pat.try_eval_event(event),
			Unknown => None,
			_ => None,
		}
	}
}

impl EvaluateEvent<f32> for InputPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<f32>> {
		EvaluateEvent::<NN64>::try_eval_event(self, event)
			.map(|val| val.convert_with(|val| *val as f32))
	}
}

impl EvaluateEvent<NN32> for InputPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN32>> {
		EvaluateEvent::<NN64>::try_eval_event(self, event)
			.map(|val| val.convert_with(|val| NN32::new(*val as f32).unwrap()))
	}
}

impl EvaluateEvent<NN64> for InputPat {
	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN64>> {
		use InputPat::*;
		match self {
			Action(pat) => pat.try_eval_event(event),
			JoypadButton(pat) => pat.try_eval_event(event),
			JoypadMotion(pat) => pat.try_eval_event(event),
			Midi(_pat) => todo!("eval MIDI events"),
			ScreenDrag(pat) => pat.try_eval_event(event),
			ScreenTouch(pat) => pat.try_eval_event(event),
			WithModifiers(pat) => pat.try_eval_event(event),
			Gesture(pat) => pat.try_eval_event(event),
			MagnifyGesture(pat) => pat.try_eval_event(event),
			PanGesture(pat) => pat.try_eval_event(event),
			Key(pat) => pat.try_eval_event(event),
			Mouse(_pat) => None,
			MouseButton(pat) => pat.try_eval_event(event),
			MouseMotion(pat) => pat.try_eval_event(event),
			Unknown => None,
		}
	}
}
