use gdnative::{
	api::{
		GlobalConstants, Input as GDInput, InputEvent, InputEventGesture, InputEventJoypadButton,
		InputEventJoypadMotion, InputEventKey, InputEventMIDI, InputEventMagnifyGesture,
		InputEventMouse, InputEventMouseButton, InputEventMouseMotion, InputEventPanGesture,
		InputEventScreenDrag, InputEventScreenTouch, InputEventWithModifiers, Viewport,
	},
	core_types::Vector2,
	object::SubClass,
	GodotObject,
};
use num_traits::Float;

use crate::{
	patterns::{
		action::ActionPat,
		deadzone::Deadzone,
		joy::{JoypadButtonPat, JoypadMotionPat},
		midi::MidiPat,
		pointer::PointerPat,
		touch::{ScreenDragPat, ScreenTouchPat},
		vec2::Vec2Pat,
		w_mods::{
			gestures::{GesturePat, MagnifyGesturePat, PanGesturePat},
			key::KeyPat,
			mouse::{MouseButtonPat, MouseMotionPat, MousePat},
			ModifierKeys,
		},
		InputPat,
	},
	utils::*,
	NN64,
};

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum SampleValue<T> {
	InRange(T),
	/// Input value is out of the range specified in the pattern.
	///
	/// In some cases this could be a bug, but often it just means that the last-known value
	/// or a default should be used instead.
	OutOfRange(T),
}

impl<T> SampleValue<T> {
	#[inline]
	pub fn map(self, f: impl FnOnce(T) -> T) -> Self {
		use SampleValue::*;
		match self {
			InRange(val) => InRange(f(val)),
			OutOfRange(val) => OutOfRange(val),
		}
	}

	#[inline]
	pub fn and_then(self, f: impl FnOnce(T) -> Self) -> Self {
		use SampleValue::*;
		match self {
			InRange(val) => f(val),
			oor => oor,
		}
	}

	#[inline]
	pub fn map_oor(self, f: impl FnOnce(T) -> T) -> Self {
		use SampleValue::*;
		match self {
			InRange(val) => InRange(val),
			OutOfRange(val) => OutOfRange(f(val)),
		}
	}

	#[inline]
	pub fn convert_with<U>(self, f: impl FnOnce(T) -> U) -> SampleValue<U> {
		use SampleValue::*;
		match self {
			InRange(val) => InRange(f(val)),
			OutOfRange(val) => OutOfRange(f(val)),
		}
	}

	#[inline]
	pub fn filter(self, f: impl FnOnce(&T) -> bool) -> Self {
		self.and_then(|val| val.check_in_range(f))
	}

	#[inline]
	pub fn deadzone(self, dz: &impl Deadzone<T>) -> Self {
		self.and_then(|val| dz.mask(val))
	}

	#[inline]
	pub fn unwrap_or(self, default: T) -> T {
		match self {
			Self::InRange(val) => val,
			_ => default,
		}
	}

	#[inline]
	pub fn unwrap_or_else(self, default: impl FnOnce(T) -> T) -> T {
		match self {
			Self::InRange(val) => val,
			Self::OutOfRange(val) => default(val),
		}
	}

	#[inline]
	pub fn get(self) -> T {
		match self {
			Self::InRange(val) | Self::OutOfRange(val) => val,
		}
	}

	#[inline]
	pub fn is_in_range(&self) -> bool {
		matches!(self, Self::InRange(_))
	}

	#[inline]
	pub fn is_out_of_range(&self) -> bool {
		matches!(self, Self::OutOfRange(_))
	}
}

pub trait IntoSampleValue: Sized {
	#[inline]
	fn in_range(self) -> SampleValue<Self> {
		SampleValue::InRange(self)
	}
	#[inline]
	fn out_of_range(self) -> SampleValue<Self> {
		SampleValue::OutOfRange(self)
	}
	#[inline]
	fn in_range_if(self, in_range: bool) -> SampleValue<Self> {
		if in_range {
			self.in_range()
		} else {
			self.out_of_range()
		}
	}
	#[inline]
	fn check_in_range(self, f: impl FnOnce(&Self) -> bool) -> SampleValue<Self> {
		let in_range = f(&self);
		self.in_range_if(in_range)
	}
}

impl<T> IntoSampleValue for T {}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum SampleError {
	/// The provided pattern is of a type that cannot be sampled at arbitrary intervals. Rather, it can
	/// only be used to match against `InputEvent`s.
	CannotSample {
		reason: &'static str,
	},
	WrongEventType,
	/// An unknown pattern was provided.
	///
	/// Likely either acquired from an unsupported Godot version, or
	/// deserialized from unrecognized data.
	UnknownPattern,
}

impl SampleError {
	#[inline]
	pub const fn cannot_sample(reason: &'static str) -> Self {
		Self::CannotSample { reason }
	}
}

pub type SampleResult<T> = Result<SampleValue<T>, SampleError>;

// === Input singleton sampling ===

/// Convenience function to sample the `Input` singleton and the root `Viewport`.
// TODO: Unfortunate that a convenience fn needs to be `unsafe`, but I don't think it's a good idea to hide it...
/// Sample the global `Input` singleton and root `Viewport`.
///
/// # Safety
/// See [root_viewport]
#[inline]
pub unsafe fn sample_input<T, Pat: SampleInput<T>>(pat: Pat) -> SampleValue<T> {
	let vp = root_viewport().assume_safe();
	pat.sample(GDInput::godot_singleton(), &vp)
}

pub trait SampleInput<T> {
	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<T> {
		self.try_sample(input, vp).unwrap()
	}

	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<T> {
		Ok(self.sample(input, vp))
	}
}

pub trait EventPattern {
	type Event: SubClass<InputEvent>;
}

impl<P: EventPattern> EventPattern for &P {
	type Event = P::Event;
}

macro_rules! impl_event_pattern {
	($($Pat:ty => $Event:ty ,)*) => {
		$(
			impl EventPattern for $Pat {
				type Event = $Event;
			}
		)*
	}
}

impl_event_pattern!(
	ActionPat => InputEvent,
	JoypadButtonPat => InputEventJoypadButton,
	JoypadMotionPat => InputEventJoypadMotion,
	MidiPat => InputEventMIDI,
	ScreenDragPat => InputEventScreenDrag,
	ScreenTouchPat => InputEventScreenTouch,
	ModifierKeys => InputEventWithModifiers,
	GesturePat => InputEventGesture,
	MagnifyGesturePat => InputEventMagnifyGesture,
	PanGesturePat => InputEventPanGesture,
	KeyPat => InputEventKey,
	MousePat => InputEventMouse,
	MouseButtonPat => InputEventMouseButton,
	MouseMotionPat => InputEventMouseMotion,

	InputPat => InputEvent,
	PointerPat => InputEvent,
	// CursorPat => InputEvent,
);

pub trait EvaluateEvent<T>: EventPattern {
	fn eval_event(&self, event: &Self::Event) -> SampleValue<T> {
		self.try_eval_event(event).unwrap()
	}

	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<T>> {
		event
			.upcast::<InputEvent>()
			.cast::<Self::Event>()
			.map(|event| self.eval_event(event))
	}
}

// impl<T: EvaluateEvent<f64>> EvaluateEvent<f32> for T {
// 	#[inline]
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<f32> {
// 		<Self as EvaluateEvent<f64>>::eval_event(self, event).convert_with(|val| val as f32)
// 	}
//
// 	#[inline]
// 	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<f32>> {
// 		<Self as EvaluateEvent<f64>>::try_eval_event(self, event)
// 			.map(|val| val.convert_with(|val| val as f32))
// 	}
// }
//
// impl<T: EvaluateEvent<NN64>> EvaluateEvent<f64> for T {
// 	#[inline]
// 	fn eval_event(&self, event: &Self::Event) -> SampleValue<f64> {
// 		<Self as EvaluateEvent<NN64>>::eval_event(self, event).convert_with(|val| *val)
// 	}
//
// 	#[inline]
// 	fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<f64>> {
// 		<Self as EvaluateEvent<NN64>>::try_eval_event(self, event)
// 			.map(|val| val.convert_with(|val| *val))
// 	}
// }

macro_rules! impl_sample_f64_via_bool {
	($($T:ty ,)*) => {
		$(
			impl SampleInput<NN64> for $T {
				#[inline]
				fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<NN64> {
					<$T as SampleInput<bool>>::sample(self, input, vp).convert_with(
						|val| val.one_or_zero()
					)
				}

				#[inline]
				fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<NN64> {
					<$T as SampleInput<bool>>::try_sample(self, input, vp)
						.map(|val| val.convert_with(|val| val.one_or_zero()))
				}
			}

			impl EvaluateEvent<NN64> for $T {
				fn eval_event(&self, event: &Self::Event) -> SampleValue<NN64> {
					<Self as EvaluateEvent<bool>>::eval_event(self, event).convert_with(|val| val.one_or_zero())
				}

				fn try_eval_event(&self, event: &impl SubClass<InputEvent>) -> Option<SampleValue<NN64>> {
					<Self as EvaluateEvent<bool>>::try_eval_event(self, event)
						.map(|val| val.convert_with(|val| val.one_or_zero()))
				}
			}
		)*
	}
}

impl_sample_f64_via_bool!(
	JoypadButtonPat,
	ScreenTouchPat,
	ModifierKeys,
	KeyPat,
	// MousePat,
	MouseButtonPat,
);

pub fn cant_sample(reason: &'static str) -> SampleError {
	SampleError::cannot_sample(reason)
}

// impl<T: SampleInput<f64>> SampleInput<f32> for T {
// 	#[inline]
// 	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<f32> {
// 		<Self as SampleInput<f64>>::sample(self, input, vp).convert_with(|val| val as f32)
// 	}
//
// 	#[inline]
// 	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<f32> {
// 		<Self as SampleInput<f64>>::try_sample(self, input, vp)
// 			.map(|val| val.convert_with(|val| val as f32))
// 	}
// }
//
// impl<T: SampleInput<NN64>> SampleInput<f64> for T {
// 	fn sample(&self, input: &GDInput, vp: &Viewport) -> SampleValue<f64> {
// 		<Self as SampleInput<NN64>>::sample(self, input, vp).convert_with(|val| *val)
// 	}
//
// 	#[inline]
// 	fn try_sample(&self, input: &GDInput, vp: &Viewport) -> SampleResult<f64> {
// 		<Self as SampleInput<NN64>>::try_sample(self, input, vp)
// 			.map(|val| val.convert_with(|val| *val))
// 	}
// }

impl ModifierKeys {
	#[inline]
	pub fn check_all(&self, input: &GDInput) -> bool {
		self.alt.matches_state(input.is_key_pressed(GlobalConstants::KEY_ALT))
			&& self.command.matches_state(input.is_key_pressed(GlobalConstants::KEY_META)) // command is meta on MacOS
			&& self.control.matches_state(input.is_key_pressed(GlobalConstants::KEY_CONTROL))
			&& self.meta.matches_state(input.is_key_pressed(GlobalConstants::KEY_META))
			&& self.shift.matches_state(input.is_key_pressed(GlobalConstants::KEY_SHIFT))
	}
}

impl SampleInput<Vector2> for Vec2Pat {
	#[inline]
	fn try_sample(&self, input: &gdnative::api::Input, vp: &Viewport) -> SampleResult<Vector2> {
		let mut in_range = true;

		let mut sample = |pat: &Option<InputPat>| {
			Ok(match pat {
				Some(pat) => pat.try_sample(input, vp)?.unwrap_or_else(|val| {
					in_range = false;
					val
				}),
				None => 0.0,
			})
		};

		Ok(match self {
			Vec2Pat::HalfAxes {
				up,
				down,
				left,
				right,
			} => {
				let up = sample(up)?.abs();
				let down = sample(down)?.abs();
				let left = sample(left)?.abs();
				let right = sample(right)?.abs();
				Vector2 {
					x: right - left,
					y: down - up, // +y is down in Godot
				}
				.in_range_if(in_range)
			}
			Vec2Pat::FullAxes {
				x,
				invert_x,
				y,
				invert_y,
			} => {
				let x_val = sample(x)?;
				let y_val = sample(y)?;
				let x = if *invert_x { -x_val } else { x_val };
				let y = match y {
					Some(
						InputPat::MouseMotion(_) // comes directly from screen
						| InputPat::ScreenTouch(_) // comes directly from screen
						| InputPat::JoypadMotion(_) // Godot apparently inverts this
					) if *invert_y => -y_val,
					Some(_) if !*invert_y => -y_val, // need to manually invert button values
					_ => y_val,
				};
				Vector2 { x, y }.in_range_if(in_range)
			}
			Vec2Pat::Direct {
				pat,
				invert_x,
				invert_y,
			} => pat
				.try_sample(input, vp)?
				.convert_with(|mut value: Vector2| {
					if *invert_x {
						value.x = -value.x
					};
					if *invert_y {
						value.y = -value.y
					};
					value
				}),
		})
	}
}
